package gateway

import (
	"context"
	"fmt"

	users "revel-vuetify-admin/gateway/admin/users/v1"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/revel/revel"
	"google.golang.org/grpc"
)

const (
	max = 1024 * 1024 * 1024
)

var (
	Mux = runtime.NewServeMux()
)

func Init() {
	ctx := context.Background()
	// ctx, cancel := context.WithCancel(ctx)
	// defer cancel()

	adminGrpcServer := fmt.Sprintf("%s:%s",
		revel.Config.StringDefault("grpc.server", ""),
		revel.Config.StringDefault("grpc.port", ""))

	opts := []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(max)),
	}

	// register gRPC server endpoint
	var err error
	endpoints := map[string][]interface{}{
		adminGrpcServer: {
			users.RegisterUsersHandlerFromEndpoint,
		},
	}
	for endpoint, handlers := range endpoints {
		for _, f := range handlers {
			err = f.(func(context.Context, *runtime.ServeMux, string, []grpc.DialOption) error)(ctx, Mux, endpoint, opts)
			if err != nil {
				revel.AppLog.Errorf("Failed register handlers from endpoint: %v", err)
				panic(err)
			}
		}
	}
}
