const path = require('path');

const PROXY_TIMEOUT = 60*60*1000;

module.exports = {
  transpileDependencies: ['vuetify'],
  lintOnSave: false,
  assetsDir: 'public/',
  configureWebpack: {
    devtool: 'source-map',
    resolve: {
      alias: {
        'leaflet': path.resolve(__dirname, 'node_modules/leaflet'),
        'vuetify': path.resolve(__dirname, 'node_modules/vuetify'),
        'ag-grid-community': path.resolve(__dirname, 'node_modules/ag-grid-community'),
        'ag-grid-enterprise': path.resolve(__dirname, 'node_modules/ag-grid-enterprise')
      }
    },
    // optimization: {
    //   runtimeChunk: 'single',
    //   splitChunks: {
    //     chunks: 'all',
    //     maxInitialRequests: Infinity,
    //     minSize: 0,
    //     cacheGroups: {
    //       vendor: {
    //         test: /[\\/]node_modules[\\/]/,
    //         name (module) {
    //           const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
    //           return `npm.${packageName.replace('@', '')}`;
    //         }
    //       }
    //     }
    //   }
    // }
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:9090',
        changeOrigin: true,
        timeout: PROXY_TIMEOUT,
        proxyTimeout: PROXY_TIMEOUT,
      },
      '/public': {
        target: 'http://localhost:9090',
        changeOrigin: true,
        timeout: PROXY_TIMEOUT,
        proxyTimeout: PROXY_TIMEOUT,
      }
    }
  },
  // chainWebpack: config => {
  //   // prevent webpack preload plugin from adding a prefetch tag to all async chunks
  //   config.plugins.delete('prefetch');
  //   config.plugin('VuetifyLoaderPlugin').tap(args => [{
  //     match (originalTag, { kebabTag, camelTag, path, component }) {
  //       if (kebabTag.startsWith('core-')) {
  //         return [camelTag, `import ${camelTag} from '@/components/core/${camelTag.substring(4)}.vue'`]
  //       }
  //     }
  //   }]);
  // }
};
