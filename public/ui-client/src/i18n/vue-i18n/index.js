import Vue from 'vue'
import VueI18n from 'vue-i18n'

import vuetifyLocales from '@/i18n/vue-i18n/vuetify'

import en from './en'
import zhHans from './zh-hans'

Vue.use(VueI18n)

const messages = {
  en: {
    ...en,
    $vuetify: vuetifyLocales.en,
  },
  zhHans: {
    ...zhHans,
    $vuetify: vuetifyLocales.zhHans,
  },
}

/**
 * VueI18n instance
 */
const i18n = new VueI18n({
  locale: 'en', // TODO: get user's locale from settings in the future
  // set locale messages
  messages,
})

export async function setLocale(locale) {
  if (i18n.locale !== locale) {
    console.log(`[Locale] Set to "${locale}"`)
    i18n.locale = locale || 'en' // TODO: or get user's locale from settings in the future
  } else {
    console.warn(`[Locale] "${locale}" is current`)
  }
}

export default i18n
