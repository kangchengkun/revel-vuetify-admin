export default {
  title: '主页',
  sections: {
    hello: '你好',
    welcome: '欢迎来到Revel Vuetify后台管理应用, 让我们开始今天的工作吧.',
    traffic: '今日流量',
    totalUsers: '总用户量',
    favorableRate: '好评率',
    negativeRate: '差评率',
    um: '用户管理',
    rm: '角色管理',
    mm: '菜单管理',
    functions: '功能模块',
    music: '音乐',
    tutorial: '教程',
  },
}
