export default {
  title: '关于我们',
  sections: {
    github: 'GitHub相关信息',
    members: '团队成员',
    commits: '提交历史',
    loadMore: '加载更多',
  },
}
