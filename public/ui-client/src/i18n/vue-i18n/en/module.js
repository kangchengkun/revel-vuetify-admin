export default {
  title: 'The module page title',
  subPage: {
    title: 'The sub page title',
    desc: 'The description for this page',
  },
}
