export default {
    title: 'Server Status',
    sections: {
        runtime: 'Runtime',
        disk: 'Disk',
        cpu: 'CPU',
        ram: 'RAM',
    },
}
