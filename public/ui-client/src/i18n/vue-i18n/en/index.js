import home from './home'
import about from './about'
import system from './system'
import module from './module'

export default {
  home,
  about,
  system,
  module,
  navHeader: {
    explore: 'Explore',
  },
  navDrawer: {},
  login: {
    signIn: 'Sign in',
    logout: 'Logout',
    account: 'Account ID',
    password: 'Password',
  },
  errors: {
    401: 'Unauthorized',
    403: 'Forbidden',
    404: 'Not Found',
    500: 'Internal Server Error',
  },
  homePage: {},
}
