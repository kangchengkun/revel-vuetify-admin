import en from '@/i18n/vuetify/en/'
import zhHans from '@/i18n/vuetify/zh-hans/'

const locales = { en, zhHans }

export default locales