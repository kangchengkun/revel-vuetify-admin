export default {
  title: 'About Us',
  sections: {
    github: 'GitHub Information',
    members: 'Team Members',
    commits: 'Commits Histories',
    loadMore: 'Load More',
  },
}
