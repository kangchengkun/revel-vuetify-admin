export default {
  title: 'Home',
  sections: {
    hello: 'Hello',
    welcome: "Welcome to Revel Vuetify Admin App, let's start your work.",
    traffic: "Today's Traffic",
    totalUsers: 'Total Users',
    favorableRate: 'Favorable Rate',
    negativeRate: 'Negative Rate',
    um: 'Users Management',
    rm: 'Roles Management',
    mm: 'Menu Management',
    functions: 'Functions',
    music: 'Music',
    tutorial: 'Tutorial',
  },
}
