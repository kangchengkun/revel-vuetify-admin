export default {
    title: '服务器状态',
    sections: {
        runtime: '运行时环境',
        disk: '存储空间',
        cpu: 'CPU',
        ram: 'RAM',
    },
}
