import home from './home'
import about from './about'
import system from './system'
import module from './module'
import zhHans from 'vuetify/lib/locale/zh-Hans'

export default {
  ...zhHans,

  home,
  about,
  system,
  module,

  navHeader: {
    explore: '探索模块',
  },
  navDrawer: {},
  login: {
    signIn: '登陆',
    logout: '登出',
    account: '账号ID',
    password: '密码',
  },
  errors: {
    401: '未授权',
    403: '禁止访问',
    404: '资源未找到',
    500: '系统内部错误',
  },
  homePage: {},
}
