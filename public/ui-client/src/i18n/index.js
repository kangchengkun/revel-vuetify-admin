/**
 * Available locales
 */
export const locales = [
  {
    title: 'English',
    locale: 'en',
    abbr: 'ENG',
  },
  {
    title: '简体中文',
    locale: 'zhHans',
    abbr: 'CHN',
  },
]
