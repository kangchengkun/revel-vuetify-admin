export const StatusCode = Object.freeze({
  OK: 2000,
  BAD_REQUEST: 4000,
  UNAUTHENTICATED: 4001,
  SESSION_EXPIRED: 4002,
  NOT_FOUND: 4004,
  INTERNAL_SERVER_ERROR: 5000,
  GRPC_SERVICE_ERROR: 6000,
  UNKNOWN_ERROR: 7000,
})

export const StatusMessage = Object.freeze({
  [StatusCode.OK]: 'Success',
  [StatusCode.BAD_REQUEST]: 'The request cannot be completed due to invalid syntax or arguments.',
  [StatusCode.UNAUTHENTICATED]: 'User is unauthenticated, please authenticate and try again.',
  [StatusCode.SESSION_EXPIRED]: 'Your session has expired. Please login again.',
  [StatusCode.NOT_FOUND]: 'The requested content is not found.',
  [StatusCode.INTERNAL_SERVER_ERROR]: 'An error occurred on the server when processing the URL. Please contact system administrator.',
  [StatusCode.GRPC_SERVICE_ERROR]: 'An error occurred when calling a gRPC service. Please contact system administrator.' ,
  [StatusCode.UNKNOWN_ERROR]: 'A unknown issue occurred. Please contact system administrator.',
})

export const getStatusText = function(statusCode) {
  if (StatusMessage.hasOwnProperty(statusCode)) {
    return StatusMessage[statusCode]
  } else {
    throw new Error('Status code does not exist: ', statusCode)
  }
}
