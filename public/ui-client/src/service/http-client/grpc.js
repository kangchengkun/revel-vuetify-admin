import axios from 'axios'
import store from '@/store'
import { StatusCode, getStatusText } from '@/service/http-client/status'

const baseClient = axios.create({
  baseURL:
    process.env.NODE_ENV === 'development'
      ? '/api'
      : process.env.VUE_APP_API_ENDPOINT + '/api',
  // timeout: 30000, // default is 0, means no timeout
  headers: {
    // 'Content-Type': 'application/json',
    // 'Authorization': `Basic ${token}`,
    // 'X-CSRF-TOKEN': '',
  },
})

const client = axios.create(baseClient.defaults)

client.interceptors.request.use(
  function (config) {
    const user = store.getters['user/user']
    config.data = Object.assign(config.data || {}, { user })
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

client.interceptors.response.use(
  function (response) {
    let { status, ...result } = response.data
    if (status) {
        if (status.code == StatusCode.OK) {
            return result
        } else {
            console.error(status)
            return Promise.reject(getStatusText(status.code))
        }
    } else {
        return Promise.reject(getStatusText(StatusCode.UNKNOWN_ERROR))
    }
  },
  function (error) {
    return Promise.reject(error)
  }
)

export { baseClient, client }
