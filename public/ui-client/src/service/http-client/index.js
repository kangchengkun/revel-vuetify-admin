import axios from 'axios'
import store from '@/store'
import { baseClient } from '@/service/http-client/grpc'

const client = axios.create(baseClient.defaults)

client.interceptors.request.use(
  function (config) {
    const user = store.getters['user/user']
    config.data = Object.assign(config.data || {}, { user })
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

client.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    return Promise.reject(error)
  }
)

export { client }
