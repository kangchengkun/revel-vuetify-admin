/**
 * Returns true if the given column is the first visible column. Usage:
 * 1. Used by checkboxSelection and headerCheckboxSelection to add checkbox
 * to the header and each selectable row in the first column.
 * 
 * defaultColDef: {
 *  checkboxSelection: isFirstColumn
 *  headerCheckboxSelection: isFirstColumn,
 * }
 * 
 * @param {*} params 
 */
export function isFirstColumn(params) {
  const displayedColumns = params.columnApi.getAllDisplayedColumns()
  const thisIsFirstColumn = displayedColumns[0] === params.column
  return thisIsFirstColumn
}

/**
 * @param {*} params 
 * Returns the output of valueFormatter if valueFormatter is provided while 
 * the valueGetter is not. Otherwise, returns params.value
 */
export function processCellCallback(params) {
  const colDef = params.column.getColDef()
  if (colDef.valueFormatter && !colDef.valueGetter) {
    const valueFormatterParams = {
      ...params,
      data: params.node.data,
      colDef: params.column.getColDef(),
    }
    return colDef.valueFormatter(valueFormatterParams)
  }
  return params.value
}