// All common configurations for ag grid tables
// For icons supported plz refer https://materialdesignicons.com/
// For colors supported plz refer https://vuetifyjs.com/zh-Hans/styles/colors/#material-82725f698868

// The reason is we should keep same style for all ranking cells
export const AG_TABLE_RANK_ICONS_CONFIG = [
  {
    rank: 1,
    attrs: {
      icon: 'mdi-star-circle',
      color: '#FFD700',
    },
  },
  {
    rank: 2,
    attrs: {
      icon: 'mdi-star-circle',
      color: '#C0C0C0',
    },
  },
  {
    rank: 3,
    attrs: {
      icon: 'mdi-star-circle',
      color: '#CD7F32',
    },
  },
]

export const AG_TABLE_COLORS = {
  PINNED_CELL: 'blue lighten-0',
  PROGRESS_CELL: 'blue lighten-5',
}