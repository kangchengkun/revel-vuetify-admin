/**
 * Truncate long string with ellipsis.
 * @param {*} maxLength - the maximum length of a string
 */
export function longTextFormatter(maxLength = 200) {
  return function(params) {
    if (typeof params.value !== 'string') {
      return params.value
    }

    let formattedValue =
      params.value.length > maxLength
        ? params.value.substring(0, maxLength)
        : params.value

    // avoid last word from being cut off
    formattedValue =
      formattedValue.length === maxLength
        ? formattedValue.substring(
            0,
            Math.min(formattedValue.length, formattedValue.lastIndexOf(' '))
          ) + ' ...'
        : formattedValue
    return formattedValue
  }
}