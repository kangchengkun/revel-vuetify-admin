import {
  snakeCaseToWhiteSpace,
  firstLetterUpperCase,
  parseJSON,
  isEmptyArray,
} from '@/utils'
import { find, cloneDeep } from 'lodash'
import { FILTER_CONDITION_SPEC } from '@/components/general/tables/value-formatters/criteria-formatter/config'

const filterConditionSpec = cloneDeep(FILTER_CONDITION_SPEC)

export class CriteriaFormatter {
  constructor(criteria) {
    this.fullExpression = this.formatCriteria(criteria)
  }
  sortCriteria(criteriaObj) {
    if (!criteriaObj || typeof criteriaObj !== 'object') return []

    const criteriaList = []
    Object.keys(criteriaObj).forEach((item) => {
      if (typeof criteriaObj[item] === 'object') {
        criteriaList.push({
          name: item,
          value: this.formatCriteria(criteriaObj[item]),
        })
      } else {
        criteriaList.push({
          name: item,
          value: criteriaObj[item],
        })
      }
    })

    criteriaList.sort((a, b) => {
      let prevCriteria = find(filterConditionSpec, { name: a.name })
      let nextCriteria = find(filterConditionSpec, { name: b.name })

      if (prevCriteria && nextCriteria) {
        return prevCriteria.id - nextCriteria.id
      } else {
        return 0
      }
    })
    return criteriaList
  }
  classifyCriteria(criteriaList) {
    const classifiedCriteria = {
      rangeCriteria: [],
      normalCriteria: [],
      primaryOtherCriteria: [],
      rangeWithTagCriteria: [],
    }
    if (isEmptyArray(criteriaList)) return classifiedCriteria

    criteriaList.forEach((criteria) => {
      if (
        criteria.name.includes('primary') ||
        criteria.name.includes('other')
      ) {
        classifiedCriteria.primaryOtherCriteria.push(criteria)
      } else if (
        criteria.name.includes('actual') ||
        criteria.name.includes('anticipate')
      ) {
        classifiedCriteria.rangeWithTagCriteria.push(criteria)
      } else if (
        criteria.name.includes('max') ||
        criteria.name.includes('min')
      ) {
        classifiedCriteria.rangeCriteria.push(criteria)
      } else {
        classifiedCriteria.normalCriteria.push(criteria)
      }
    })
    return classifiedCriteria
  }
  generateNormalExpression(normalCriteria) {
    const normalCriteriaExp = []
    if (isEmptyArray(normalCriteria)) return normalCriteriaExp

    normalCriteria.forEach((criteria) => {
      let criteriaConfig = find(filterConditionSpec, { name: criteria.name })
      let expression
      if (Array.isArray(criteria.value) && criteria.value.length > 1) {
        let nameExpression = `${firstLetterUpperCase(
          snakeCaseToWhiteSpace(criteria.name)
        )} ${criteriaConfig ? criteriaConfig.relationSymbol : 'is'}`
        let valueExpression = criteria.value.join(
          ` ${criteriaConfig ? criteriaConfig.joinedBy : 'AND'} `
        )
        expression = `[(${nameExpression} ${valueExpression})]`
      } else {
        expression = `[(${firstLetterUpperCase(
          snakeCaseToWhiteSpace(criteria.name)
        )} ${criteriaConfig ? criteriaConfig.relationSymbol : 'is'} ${
          criteria.value
        })]`
      }
      normalCriteriaExp.push(expression)
    })
    return normalCriteriaExp
  }
  generateRangeExpression(rangeCriteria) {
    let expression = ''
    const rangeCriteriaExp = []
    if (isEmptyArray(rangeCriteria)) return rangeCriteriaExp

    rangeCriteria.forEach((criteria, index) => {
      if (criteria.name.includes('min')) {
        if (
          index < rangeCriteria.length - 1 &&
          criteria.name === rangeCriteria[index + 1].name.replace('max', 'min')
        ) {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(
              criteria.name.substring(0, criteria.name.lastIndexOf('_'))
            )
          )} is from ${criteria.value} `
        } else {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(criteria.name)
          )} is from ${criteria.value})]`
        }
      } else {
        if (
          index > 0 &&
          criteria.name === rangeCriteria[index - 1].name.replace('min', 'max')
        ) {
          expression = `to ${criteria.value})]`
          rangeCriteriaExp[rangeCriteriaExp.length - 1] += expression
          return
        } else {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(
              criteria.name.substring(0, criteria.name.lastIndexOf('_'))
            )
          )} is to ${criteria.value})]`
        }
      }
      rangeCriteriaExp.push(expression)
    })
    return rangeCriteriaExp
  }
  generatePrimaryOtherExpression(rangeCriteria, primaryOtherCriteria) {
    let expression = ''
    const primaryOtherCriteriaExp = []
    if (isEmptyArray(primaryOtherCriteria)) return primaryOtherCriteria

    primaryOtherCriteria.forEach((criteria, index) => {
      if (criteria.name.includes('primary')) {
        if (
          index < rangeCriteria.length - 1 &&
          criteria.name ===
            rangeCriteria[index + 1].name.replace('other', 'primary')
        ) {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(criteria.name)
          )} is ${criteria.value.join(' OR ')}) OR `
        } else {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(criteria.name)
          )} is ${criteria.value.join(' OR ')})]`
        }
      } else {
        if (
          index > 0 &&
          criteria.name ===
            primaryOtherCriteria[index - 1].name.replace('primary', 'other')
        ) {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(criteria.name)
          )} is ${criteria.value.join(' OR ')})]`
          primaryOtherCriteriaExp[
            primaryOtherCriteriaExp.length - 1
          ] += expression
          return
        } else {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(criteria.name)
          )} is ${criteria.value.join(' OR ')})]`
        }
      }
      primaryOtherCriteriaExp.push(expression)
    })
    return primaryOtherCriteriaExp
  }
  generateRangeWithTagExpression(rangeWithTagCriteria) {
    const rangeWithTagCriteriaExp = []
    if (isEmptyArray(rangeWithTagCriteria)) return rangeWithTagCriteriaExp

    rangeWithTagCriteria.forEach((criteria, index) => {
      let expression
      if (criteria.name.includes('min')) {
        if (criteria.name.includes('actual')) {
          expression = `[(${firstLetterUpperCase(
            snakeCaseToWhiteSpace(
              criteria.name.substring(0, criteria.name.lastIndexOf('_'))
            )
          )} is from ${criteria.value}`
          if (
            index < rangeWithTagCriteria.length - 1 &&
            rangeWithTagCriteria[index + 1].name.includes(
              criteria.name.substring(0, criteria.name.lastIndexOf('_'))
            ) &&
            criteria.name !==
              rangeWithTagCriteria[index + 1].name.replace('max', 'min')
          ) {
            expression += ') OR'
          }
          if (
            index === rangeWithTagCriteria.length - 1 ||
            criteria.name !==
              rangeWithTagCriteria[index + 1].name.replace('max', 'min')
          ) {
            expression += ')] '
          }
        } else {
          if (
            index > 0 &&
            rangeWithTagCriteria[index - 1].name.includes(
              criteria.name.substring(
                criteria.name.indexOf('_'),
                criteria.name.lastIndexOf('_')
              )
            )
          ) {
            rangeWithTagCriteriaExp[
              rangeWithTagCriteriaExp.length - 1
            ] += `(${firstLetterUpperCase(
              snakeCaseToWhiteSpace(
                criteria.name.substring(0, criteria.name.lastIndexOf('_'))
              )
            )} is from ${criteria.value}`

            if (
              index === rangeWithTagCriteria.length - 1 ||
              criteria.name !==
                rangeWithTagCriteria[index + 1].name.replace('max', 'min')
            ) {
              expression += ') '
            }
            return
          } else {
            expression = `(${firstLetterUpperCase(
              snakeCaseToWhiteSpace(
                criteria.name.substring(0, criteria.name.lastIndexOf('_'))
              )
            )} is from ${criteria.value}`
            if (
              index === rangeWithTagCriteria.length - 1 ||
              criteria.name !==
                rangeWithTagCriteria[index + 1].name.replace('max', 'min')
            ) {
              expression += ')'
            }
          }
        }
      } else {
        if (
          index > 0 &&
          criteria.name ===
            rangeWithTagCriteria[index - 1].name.replace('min', 'max')
        ) {
          if (
            index < rangeWithTagCriteria.length - 1 &&
            rangeWithTagCriteria[index + 1].name.includes(
              criteria.name.substring(
                criteria.name.indexOf('_'),
                criteria.name.lastIndexOf('_')
              )
            )
          ) {
            rangeWithTagCriteriaExp[
              rangeWithTagCriteriaExp.length - 1
            ] += ` to ${criteria.value}) OR `
          } else {
            rangeWithTagCriteriaExp[
              rangeWithTagCriteriaExp.length - 1
            ] += ` to ${criteria.value})]`
          }
          return
        } else {
          if (
            index < rangeWithTagCriteria.length - 1 &&
            rangeWithTagCriteria[index + 1].name.includes(
              criteria.name.substring(
                criteria.name.indexOf('_'),
                criteria.name.lastIndexOf('_')
              )
            )
          ) {
            expression = `(${firstLetterUpperCase(
              snakeCaseToWhiteSpace(
                criteria.name.substring(0, criteria.name.lastIndexOf('_'))
              )
            )} is to ${criteria.value}) OR `
          } else {
            rangeWithTagCriteriaExp[
              rangeWithTagCriteriaExp.length - 1
            ] += `(${firstLetterUpperCase(
              snakeCaseToWhiteSpace(
                criteria.name.substring(0, criteria.name.lastIndexOf('_'))
              )
            )} is to ${criteria.value}) `
            return
          }
        }
      }

      rangeWithTagCriteriaExp.push(expression)
    })
    return rangeWithTagCriteriaExp
  }
  concatAllExpressions(
    rangeCriteriaExp,
    normalCriteriaExp,
    rangeWithTagCriteriaExp,
    primaryOtherCriteriaExp
  ) {
    if (
      !Array.isArray(rangeCriteriaExp) ||
      !Array.isArray(normalCriteriaExp) ||
      !Array.isArray(rangeWithTagCriteriaExp) ||
      !Array.isArray(primaryOtherCriteriaExp)
    )
      return ''

    let allExp = normalCriteriaExp.concat(
      primaryOtherCriteriaExp,
      rangeCriteriaExp,
      rangeWithTagCriteriaExp
    )
    return allExp.join(' AND ')
  }
  formatCriteria(criteria) {
    let criteriaObj = criteria
    if (typeof criteria === 'string' && criteria.indexOf('{') > -1) {
      criteriaObj = parseJSON(
        `${criteria}`.trim().replace(/'|&#39;|&quot;/g, '"')
      )
    }
    if (!criteriaObj) return ''

    const criteriaList = this.sortCriteria(criteriaObj)
    const classifiedCriteria = this.classifyCriteria(criteriaList)
    const rangeCrtExp = this.generateRangeExpression(
      classifiedCriteria.rangeCriteria
    )
    const normalCrtExp = this.generateNormalExpression(
      classifiedCriteria.normalCriteria
    )
    const primaryOtherCrtExp = this.generatePrimaryOtherExpression(
      classifiedCriteria.rangeCriteria,
      classifiedCriteria.primaryOtherCriteria
    )
    const rangeWithTagCrtExp = this.generateRangeWithTagExpression(
      classifiedCriteria.rangeWithTagCriteria
    )
    return this.concatAllExpressions(
      rangeCrtExp,
      normalCrtExp,
      rangeWithTagCrtExp,
      primaryOtherCrtExp
    )
  }
  getFormattedValue() {
    return this.fullExpression
  }
}
