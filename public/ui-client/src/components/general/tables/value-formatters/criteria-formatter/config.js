export const FILTER_CONDITION_SPEC = [
  {
    id: 1,
    name: 'primary_drug_status',
    relationSymbol: 'is'
  },
  {
    id: 2,
    name: 'trial_country',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 3,
    name: 'trial_region',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  // task without file
  {
    id: 4,
    name: 'study_design_keyword',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  // task with file
  {
    id: 4,
    name: 'study_design_keyword_must',
    joinedBy: 'AND',
    relationSymbol: 'is'
  },
  {
    id: 5,
    name: 'study_design_keyword_optional',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 6,
    name: 'secondary_other_endpoint_summary',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 7,
    name: 'sponsor_type',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 8,
    name: 'primary_tested_drug',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 9,
    name: 'other_tested_drug',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 10,
    name: 'gender',
    relationSymbol: 'is'
  },
  {
    id: 11,
    name: 'trial_outcomes',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 12,
    name: 'phase',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 13,
    name: 'sponsor',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 14,
    name: 'trialtrove_indication',
    relationSymbol: 'is'
  },
  {
    id: 15,
    name: 'primary_mechanism_of_action',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 16,
    name: 'other_mechanism_of_action',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 17,
    name: 'meshterm',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 18,
    name: 'trialtrove_ta',
    relationSymbol: 'is'
  },
  {
    id: 19,
    name: 'primary_endpoint_summary',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 20,
    name: 'trial_tag_attribute',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  // task without file
  {
    id: 21,
    name: 'patient_segment',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  // task with file
  {
    id: 21,
    name: 'patient_segment_must',
    joinedBy: 'AND',
    relationSymbol: 'is'
  },
  {
    id: 22,
    name: 'patient_segment_optional',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 23,
    name: 'patient_segment_mustnot',
    joinedBy:'AND',
    relationSymbol: 'is'
  },
  {
    id: 24,
    name: 'trial_status',
    joinedBy: 'OR',
    relationSymbol: 'is'
  },
  {
    id: 25,
    name: 'exclusion',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 26,
    name: 'treatment_plan',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 27,
    name: 'trial_result',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 28,
    name: 'primary_endpoint_details',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 29,
    name: 'disposition_of_patients',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 30,
    name: 'outcome_details',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 31,
    name: 'full_text_search',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 32,
    name: 'secondary_other_endpoint_details',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 33,
    name: 'patient_population',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 34,
    name: 'associated_cro',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 35,
    name: 'trial_title',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 36,
    name: 'inclusion',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 37,
    name: 'study_design',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 38,
    name: 'trial_objective',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 39,
    name: 'prior_concurrent_therapy',
    joinedBy: 'OR',
    relationSymbol: 'contains'
  },
  {
    id: 40,
    name: 'actual_accrual_min',
    relationSymbol: 'is'
  },
  {
    id: 41,
    name: 'actual_accrual_max',
    relationSymbol: 'is'
  },
  {
    id: 42,
    name: 'actual_pts_site_mo_min',
    relationSymbol: 'is'
  },
  {
    id: 43,
    name: 'actual_pts_site_mo_max',
    relationSymbol: 'is'
  },
  {
    id: 44,
    name: 'actual_enrollment_close_date_min',
    relationSymbol: 'is'
  },
  {
    id: 45,
    name: 'actual_enrollment_close_date_max',
    relationSymbol: 'is'
  },
  {
    id: 46,
    name: 'anticipate_enrollment_close_date_min',
    relationSymbol: 'is'
  },
  {
    id: 47,
    name: 'anticipate_enrollment_close_date_max',
    relationSymbol: 'is'
  },
  {
    id: 48,
    name: 'actual_enrollment_duration_min',
    relationSymbol: 'is'
  },
  {
    id: 49,
    name: 'actual_enrollment_duration_max',
    relationSymbol: 'is'
  },
  {
    id: 50,
    name: 'anticipate_enrollment_duration_min',
    relationSymbol: 'is'
  },
  {
    id: 51,
    name: 'anticipate_enrollment_duration_max',
    relationSymbol: 'is'
  },
  {
    id: 52,
    name: 'actual_primary_completion_date_min',
    relationSymbol: 'is'
  },
  {
    id: 53,
    name: 'actual_primary_completion_date_max',
    relationSymbol: 'is'
  },
  {
    id: 54,
    name: 'anticipate_primary_completion_date_min',
    relationSymbol: 'is'
  },
  {
    id: 55,
    name: 'anticipate_primary_completion_date_max',
    relationSymbol: 'is'
  },
  {
    id: 56,
    name: 'actual_primary_endpoints_reported_date_min',
    relationSymbol: 'is'
  },
  {
    id: 57,
    name: 'actual_primary_endpoints_reported_date_max',
    relationSymbol: 'is'
  },
  {
    id: 58,
    name: 'anticipate_primary_endpoints_reported_date_min',
    relationSymbol: 'is'
  },
  {
    id: 59,
    name: 'anticipate_primary_endpoints_reported_date_max',
    relationSymbol: 'is'
  },
  {
    id: 60,
    name: 'actual_start_date_min',
    relationSymbol: 'is'
  },
  {
    id: 61,
    name: 'actual_start_date_max',
    relationSymbol: 'is'
  },
  {
    id: 62,
    name: 'anticipate_start_date_min',
    relationSymbol: 'is'
  },
  {
    id: 63,
    name: 'anticipate_start_date_max',
    relationSymbol: 'is'
  },
  {
    id: 64,
    name: 'actual_treatment_duration_min',
    relationSymbol: 'is'
  },
  {
    id: 65,
    name: 'actual_treatment_duration_max',
    relationSymbol: 'is'
  },
  {
    id: 66,
    name: 'anticipate_treatment_duration_min',
    relationSymbol: 'is'
  },
  {
    id: 67,
    name: 'anticipate_treatment_duration_max',
    relationSymbol: 'is'
  },
  {
    id: 68,
    name: 'identified_sites_min',
    relationSymbol: 'is'
  },
  {
    id: 69,
    name: 'identified_sites_max',
    relationSymbol: 'is'
  },
  {
    id: 70,
    name: 'last_modified_date_min',
    relationSymbol: 'is'
  },
  {
    id: 71,
    name: 'last_modified_date_max',
    relationSymbol: 'is'
  },
  {
    id: 72,
    name: 'reported_sites_min',
    relationSymbol: 'is'
  },
  {
    id: 73,
    name: 'reported_sites_max',
    relationSymbol: 'is'
  },
  {
    id: 74,
    name: 'target_accrual_min',
    relationSymbol: 'is'
  },
  {
    id: 75,
    name: 'target_accrual_max',
    relationSymbol: 'is'
  },
  {
    id: 76,
    name: 'age_min',
    relationSymbol: 'is'
  },
  {
    id: 77,
    name: 'age_max',
    relationSymbol: 'is'
  },
  {
    id: 78,
    name: 'compound',
    relationSymbol: 'is'
  }
]


