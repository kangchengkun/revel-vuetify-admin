export const TASK_FILE_DEF = {
  type: 'SingleSelect',
  dense: false,
  cols: 6,
  label: 'Select A File',
  prependIcon: 'mdi-paperclip',
  rules: [v => !!v || 'Please select or upload a file'],
  items: [],
};

export const TASK_INFO_DEF = [
  {
    type: 'TextInput',
    name: 'task_name',
    label: 'Task Name',
    dense: false,
    prependIcon: 'mdi-file-document-edit',
    rules: [v => !!v || 'Task name is required'],
  },
  {
    type: 'SingleSelect',
    name: 'ta',
    label: 'TA',
    dense: false,
    cols: 6,
    linked: true,
    dependency: 0,
    rules: [v => !!v || 'Please select a TA'],
  },
  {
    type: 'SingleSelect',
    name: 'indication',
    label: 'Indication',
    dense: false,
    cols: 6,
    rules: [v => !!v || 'Please select an indication'],
    linked: true,
    dependency: 1, // the available options of indication depends on the selection of 'ta'
  },
  {
    type: 'SingleSelect',
    name: 'study_id',
    label: 'Study ID',
    dense: false,
    rules: [v => !!v || 'Please select a study'],
    linked: true,
    dependency: 2, // the available options of study_id depends on the selection of 'ta' and 'indication'
  },
]