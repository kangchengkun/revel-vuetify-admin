import { consoleInfo } from 'vuetify/lib/util/console'

const navDrawerMixin = {
  // data() {
  //   return {
  //     menu: []
  //   }
  // },
  methods: {
    isActive(route) {
      return route.name === this.$route.name
    },
    onMenuItemClick(name) {
      consoleInfo(name)
    },
  },
}

export default navDrawerMixin
