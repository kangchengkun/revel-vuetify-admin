import { ROUTE_NAMES } from '@/router/configs/common'

export const MENU_TYPES = {
  MAIN_MENU: 'MainMenu',
}

export const DEFAULT_MENU = MENU_TYPES.MAIN_MENU

export const MAIN_MENU = [
  { name: ROUTE_NAMES.HOME },
  {
    name: ROUTE_NAMES.ABOUT,
    title: 'About Us',
    // scrollOffset: 170,
    // highlightFirstItem: true,
    // scrollContainerSelector: '#sections-container',
  },
  {
    name: ROUTE_NAMES.SYSTEM,
    title: 'Server Status',
    // scrollOffset: 170,
    // highlightFirstItem: true,
    // scrollContainerSelector: '#sections-container',
  },
]

export const TASK_MENU = []
