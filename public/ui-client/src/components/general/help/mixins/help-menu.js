const HelpMenuMixin = {
  data() {
    return {};
  },
  props: {
    itemData: {
      type: Object,
      required: true,
      validator: function (value) {
        return value && value.icon && value.text && value.key;
      },
    },
  },
  computed: {},
  created() {},
  methods: {},
};

export default HelpMenuMixin;
