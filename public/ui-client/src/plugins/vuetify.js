import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import themes from '@/plugins/vuetify-themes'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

import i18n from '@/i18n/vue-i18n/'
// import vuetifyLocales from '@/i18n/vuetify/'

export default new Vuetify({
  lang: {
    current: 'en', // TODO: get user's locale from settings in the future
    // vuetify
    // locales: vuetifyLocales,
    // vue-i18n
    t: (key, ...params) => i18n.t(key, params),
  },
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    options: {
      customProperties: true,
    },
    dark: false,
    themes: themes[0],
  },
})
