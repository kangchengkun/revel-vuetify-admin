// All constants for common usages
import {
  isValidDateStr,
  formatDateStr,
  revertFloatToInitValue,
  parseJSON,
} from '@/utils'

export const RVA_DEFAULT_PAGE_SIZE = 15
export const RVA_DEFAULT_COUNTRY_LEN = 15
export const RVA_DEFAULT_DATE_MIN_YEAR = 1800

/**
 * Event types used in RVA
 */
export const RVA_EVENT_TYPES = {
  LEFT_NAV_DRAWER_TOGGLED: 'rva.left.nav.drawer.toggled', // TOTO: need to remove maybe.
  RENDER_CONTAINER_SIZE_CHANGED: 'rva.render.container.size.changed',
  CURRENT_SAVED_SEARCH_ID_CHANGED:
    'rva.saved.searches.current.search.id.changed',
}

/**
 * pre-defined column types
 */
export const RVA_COL_TYPES = {
  DATE: 'date',
  RANK: 'rank',
  LINKS: 'links',
  COUNTRY: 'country',
  PROGRESS: 'progress',
}

/**
 * The default date filter params for ag tables
 */
export const RVA_DEFAULT_DATE_FILTER_PARAMS = {
  debounceMs: 500,
  buttons: ['clear', 'reset', 'cancel', 'apply'],
  filterOptions: [
    'equals',
    'notEqual',
    'contains',
    'notContains',
    'startsWith',
    'endsWith',
    'lessThan',
    'lessThanOrEqual',
    'greaterThan',
    'greaterThanOrEqual',
    'inRange',
  ],
  closeOnApply: true,
  inRangeInclusive: true,
  comparator: function(filterLocalDateAtMidnight, cellValue) {
    if (!isValidDateStr(cellValue)) return -1

    const dateString = new Date(formatDateStr(cellValue)).toLocaleDateString()
    const dateParts = dateString.split('/') // mm/dd/YYYY
    const cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[0]) - 1,
      Number(dateParts[1])
    )
    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0
    }
    if (cellDate < filterLocalDateAtMidnight) {
      return -1
    }
    if (cellDate > filterLocalDateAtMidnight) {
      return 1
    }
  },
  browserDatePicker: true,
  minValidYear: RVA_DEFAULT_DATE_MIN_YEAR,
}

export const RVA_DEFAULT_DATE_FORMATTER = function(params) {
  if (!params || !params.value) return ''

  const dateAsString = new Date(formatDateStr(params.value)).toUTCString()
  const tempArr = dateAsString.split(',')
  if (tempArr.length < 2) return params.value

  const dateTimeParts = tempArr[1].trim().split(' ')
  if (dateTimeParts.length < 3) return params.value

  return `${dateTimeParts[0]}-${dateTimeParts[1]}, ${dateTimeParts[2]}`
}

export const RVA_DEFAULT_COUNTRY_VALUE_GETTER = function(params) {
  if (
    !params ||
    !params.data ||
    !params.colDef ||
    !params.colDef.field ||
    !params.data[params.colDef.field]
  )
    return ''

  let countryInfo = params.data[params.colDef.field]
  if (typeof countryInfo === 'string' && countryInfo.indexOf('{') > -1) {
    countryInfo = parseJSON(
      `${countryInfo}`.trim().replace(/'|&#39;|&quot;/g, '"')
    )
  }
  if (!countryInfo) return ''

  if (typeof countryInfo === 'string') return countryInfo.toUpperCase
  if (typeof countryInfo === 'object') return JSON.stringify(countryInfo)
  return ''
}

/***
 * The custom comparator when sorting in ag table
 * @param value1
 * @param value2
 * @returns {number}
 */
export const RVA_DEFAULT_COMPARATOR = function(value1, value2) {
  if (
    (!value1 && !value2) ||
    (typeof value1 === 'object' && typeof value2 === 'object')
  ) {
    return 0
  }

  if (!value1) {
    return -1
  }

  if (!value2) {
    return 1
  }

  if (typeof value1 === 'number' && typeof value2 === 'number') {
    return value1 - value2
  }

  if (typeof value1 === 'string' && typeof value2 === 'string') {
    if (
      value1.charAt(value1.length - 1) === '%' &&
      value2.charAt(value2.length - 1) === '%'
    ) {
      const floatValue1 = parseFloat(value1.substring(0, value1.length - 1))
      const floatValue2 = parseFloat(value2.substring(0, value2.length - 1))
      if (isNaN(floatValue1) || isNaN(floatValue2)) {
        if (
          revertFloatToInitValue(value1, floatValue1) === value1 &&
          revertFloatToInitValue(value2, floatValue2) === value2
        ) {
          return floatValue1 - floatValue2
        }
        return value1.localeCompare(value2)
      }
      return floatValue1 - floatValue2
    } else {
      const floatValue1 = parseFloat(value1)
      const floatValue2 = parseFloat(value2)
      if (
        isNaN(floatValue1) ||
        isNaN(floatValue2) ||
        `${floatValue1}`.length < value1.length ||
        `${floatValue2}`.length < value2.length
      ) {
        if (
          revertFloatToInitValue(value1, floatValue1) === value1 &&
          revertFloatToInitValue(value2, floatValue2) === value2
        ) {
          return floatValue1 - floatValue2
        }
        return value1.localeCompare(value2)
      }
      return floatValue1 - floatValue2
    }
  }
  return 0
}

/***
 * The custom comparator for date columns when sorting in ag table
 * @param value1
 * @param value2
 * @returns {number}
 */
export const RVA_DEFAULT_DATE_COMPARATOR = function(date1, date2) {
  if (!isValidDateStr(date1) && !isValidDateStr(date2)) {
    return 0
  }

  if (!isValidDateStr(date1)) {
    return -1
  }

  if (!isValidDateStr(date2)) {
    return 1
  }

  return new Date(date1).getTime() - new Date(date2).getTime()
}
