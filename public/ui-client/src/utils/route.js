import store from '@/store'
import router, { routes } from '@/router'
import { has, isArray } from 'lodash'

/**
 * 
 * @param {*} routes 
 * @param {*} name 
 * @return route definition
 */
export const getRouteByName = function(routes, name) {
  if (!name || !isArray(routes)) return null
  return routes.reduce((acc, route) => {
    if (acc) return acc
    if (route.name === name) return route
    if (route.children) return getRouteByName(route.children, name)
  }, null)
}

export const getRoutesByNames = function(names, routes) {
  if (!routes) {
    routes = router.options.routes
  }
  return names.reduce((acc, name) => {
    const route = getRouteByName(routes, name)
    if (route) acc.push(route)
    return acc
  }, [])
}

/**
 * @param {*} to 
 * @return $route.params.taskId
 */
export const getRouteTaskId = function(to) {
  if (to.params && to.params.taskId) {
    return store.getters['tasks/taskName'](to.params.taskId)
  }
  return to.meta && to.meta.title || to.name
}

/**
 * @param {*} to 
 * @return name of current route's parent route
 */
export const getParentRouteName = function(to) {
  const matchedRoutes = to.matched
  if (matchedRoutes.length > 2) {
    return matchedRoutes[matchedRoutes.length - 2].name
  }
  return ''
}

export const getRouteMeta = function (route) {
  const meta = route.meta
  if (meta && meta.mergeParentMeta) {
    const parent = route.matched[route.matched.length - 1].parent
    if (parent && parent.meta) {
      return { ...parent.meta, ...meta }
    }
  }
  return meta
}

export const getMenuFromRoutes = function(configs, deep=false) {
  return configs.reduce((acc, { title, name, params, action, disabled }) => {
    const route = getRouteByName(routes, name)
    if (route) {
      const item = {
        name: route.name,
        action: action || '',
        icon: route.meta.icon,
        title: title || route.meta.title,
        disabled: (typeof disabled === 'function') ? disabled() : disabled,
        location: { 
          name: route.name, params,
          params: (typeof params === 'function') ? params() : params
        },
      }
      if (deep && has(route, 'meta.sections')) {
        Object.assign(item, {
          sections: route.meta.sections
        })
      }
      acc.push(item)
    }
    return acc
  }, [])
}
