import XLSX from 'xlsx'
import {
  RVA_COL_TYPES,
  RVA_DEFAULT_DATE_MIN_YEAR,
  RVA_DEFAULT_DATE_COMPARATOR,
  RVA_DEFAULT_DATE_FORMATTER,
  RVA_DEFAULT_DATE_FILTER_PARAMS,
  RVA_DEFAULT_COUNTRY_VALUE_GETTER,
} from '@/utils/constants'
import { isEmpty, isEqual, sortBy, find, findIndex, has, isArray } from 'lodash'

export function capitalize(value) {
  if (!value) return ''
  let words = value.toString().toLowerCase().split(' ')
  words = words.map((word) => {
    return word.charAt(0).toUpperCase() + word.slice(1)
  })
  return words.join(' ')
}

export function splitCamelCase(value) {
  if (!value) return ''
  value = value.replace(/([a-z0-9])([A-Z])/g, '$1 $2')
  return capitalize(value)
}

export function formatRoutes(routes) {
  return routes.map((item) => formatRoute(item))
}

export function formatRoute(route) {
  if (!route || !route.path || !route.component) {
    console.error(new Error('Invalid route configuration'))
    return
  }

  const tempRoute = { ...route }
  tempRoute.component = lazyLoadComponent(route.component)
  if (!isEmpty(route.components)) {
    tempRoute.components = {}
    tempRoute.meta.sections = []
    tempRoute.meta.categories = []
    route.components.forEach((item) => {
      if (item) {
        if (item.name && item.component)
          tempRoute.components[`${item.name}`] = lazyLoadComponent(
            item.component
          )
        if (item.title) tempRoute.meta.sections.push(item.title)
        if (item.name && item.title)
          tempRoute.meta.categories.push({
            title: item.title,
            component: item.name,
          })
      }
    })
  }
  if (!isEmpty(tempRoute.children)) {
    tempRoute.children = formatRoutes(route.children)
  }

  return tempRoute
}

export function lazyLoadComponent(path) {
  return () => import(/* webpackChunkName: "component-[request]" */ path)
}

export function getHash(value) {
  if (!value) return ''
  value = value.replace(/[^A-Za-z0-9]/g, ' ').replace(/  +/g, ' ')
  value = value.toLowerCase()
  return value.split(' ').join('-')
}

// Converts JSON string into an object; returns null is the text is empty or undefined
export function parseJSON(text) {
  if (!text) return null
  if (typeof text !== 'string') {
    console.error(`JSON.parse does not allow ${typeof text}`)
    return null
  }
  try {
    const result = JSON.parse(text)
    return result
  } catch (e) {
    console.error(`Parsing ${text} failed with error: ${e}`)
    return null
  }
}

// Format all of keys to upper case for a object
export function formatObjectKeys(object) {
  const tempObject = {}
  for (let key in object) {
    if (key && typeof key === 'string' && key.trim())
      tempObject[key.toUpperCase()] = object[key]
  }
  return tempObject
}

// Format country string to json object
export function parseCountryJSON(countryStr) {
  return parseJSON(countryStr.replace(/'/g, `"`))
}

export function formatId(value) {
  if (!value) return ''
  value = value.replace(/[^A-Za-z0-9]/g, ' ').replace(/  +/g, ' ')
  value = value.toLowerCase()
  return value.split(' ').join('-')
}

export function omitEmpty(object) {
  return Object.keys(object).reduce((acc, key) => {
    const value = object[key]
    if (
      typeof value === 'undefined' ||
      !value ||
      (Array.isArray(value) && value.length === 0)
    ) {
      // acc = omit(acc, [key])
      delete acc[key]
    }
    return acc
  }, object)
}

export function generateRandomKey() {
  const timestamp = new Date().getTime()
  const randomValue = Math.random().toString().split('.')[1]

  return `${timestamp}-${randomValue}`
}

export function snakeCaseToWhiteSpace(value) {
  return value.split('_').join(' ')
}

export function firstLetterUpperCase(str) {
  return str.replace(/\b(\w)(\w*)/g, function ($0, $1, $2) {
    return $1.toUpperCase() + $2.toLowerCase()
  })
}
/***
 *
 * Download the data as excel file.
 * @param excelData [Array] An array excel sheetName and data. example:
 * [{
 *   sheetData: [['test', 'test'],['1', '2']],
 *   sheetName: 'sheet1',
 *   fitColumn: true,
 *   rowOpts: [{ hpt: 25 }, { hpt: 40 }, { hpt: 40 }]
 * },
 * {
 *   sheetData: [['test1', 'test1'],['12', '22']],
 *   sheetName: 'sheet2'
 * }]
 * @param storedFileName [String]
 */
export function downloadExcel(excelData = [], storedFileName = 'export.xlsx') {
  if (isEmptyArray(excelData)) {
    console.log('There is no data provided')
    return
  }

  try {
    const sheetNames = []
    const workSheets = {}
    const fitToColumn = (data) => {
      const columnWidths = []
      for (const property in data[0]) {
        columnWidths.push({
          wch: Math.max(
            property ? property.toString().length + 2 : 0,
            ...data.map((obj) =>
              obj[property] ? obj[property].toString().length + 2 : 0
            )
          ),
        })
      }
      return columnWidths
    }
    excelData.forEach((item, index) => {
      if (!isEmptyArray(item['sheetData'])) {
        const sheetName = item['sheetName'] || `sheet${index + 1}`
        sheetNames.push(sheetName)
        if (
          item['sheetData'].length === 1 &&
          isEmptyArray(item['sheetData'][0])
        ) {
          item['sheetData'][0] = ['No data found']
        }
        let sheet = XLSX.utils.aoa_to_sheet(item['sheetData'])
        sheet['A1'].s = {
          font: {
            sz: 12,
            color: { rgb: '000000' },
            bold: true,
          },
          alignment: { horizontal: 'center', vertical: 'center' },
        }
        if (item.fitColumn) sheet['!cols'] = fitToColumn(item['sheetData'])
        if (item.rowOpts) sheet['!rows'] = item.rowOpts
        workSheets[sheetName] = sheet
      }
    })
    let url = sheetsToBlob(sheetNames, workSheets)
    if (typeof url == 'object' && url instanceof Blob) {
      url = URL.createObjectURL(url)
    }
    let aLink = document.createElement('a')
    aLink.href = url.toString()
    aLink.download = storedFileName
    let event
    if (window.MouseEvent) {
      event = new MouseEvent('click')
    } else {
      event = document.createEvent('MouseEvents')
      event.initMouseEvent(
        'click',
        true,
        false,
        window,
        0,
        0,
        0,
        0,
        0,
        false,
        false,
        false,
        false,
        0,
        null
      )
    }
    aLink.dispatchEvent(event)
  } catch (e) {
    console.error(e)
    throw new Error(e)
  }
}

export function sheetsToBlob(sheetNames, workSheets) {
  if (isEmptyArray(sheetNames) || isEmptyArray(sheetNames)) return

  let workbook = {
    SheetNames: sheetNames,
    Sheets: workSheets,
  }
  let outStream = XLSX.write(workbook, {
    bookType: 'xlsx',
    bookSST: false,
    type: 'binary',
  })

  let blob = new Blob([strToaArrayBuff(outStream)], {
    type: 'application/octet-stream',
  })

  function strToaArrayBuff(s) {
    let buf = new ArrayBuffer(s.length)
    let view = new Uint8Array(buf)
    for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff
    return buf
  }
  return blob
}

/**
 * Check an object is an Empty array or not
 * @param object
 * @returns {boolean}
 */
export function isEmptyArray(object) {
  return !object || !Array.isArray(object) || object.length === 0
}

/**
 * Check whether an object is a valid string or not
 * @param object
 * @returns {*|boolean}
 */
export function isValidString(object) {
  return object && typeof object === 'string' && object.trim().length > 0
}

export function revertFloatToInitValue(value, floatValue) {
  let tempValueStr = Number.isInteger(floatValue)
    ? `${floatValue}.`
    : `${floatValue}`
  let diffLen =
    value.indexOf('.') >= 0
      ? value.length - `${floatValue}`.length - 1
      : value.length - `${floatValue}`.length
  for (let i = 0; i < diffLen; i++) {
    tempValueStr += '0'
  }
  return tempValueStr
}

export function timeFormatter(value) {
  const dateTime = new Date(value)
  if (Date.parse(dateTime) < 0) {
    return ''
  }
  const year = dateTime.getFullYear()
  const month = ('0' + (dateTime.getMonth() + 1)).slice(-2)
  const date = ('0' + (dateTime.getDate() + 1)).slice(-2)
  return `${year}-${month}-${date}`
}

// Performs a deep comparison between two arrays and returns true if they are equivalent
export function isArrayEqual(arr1, arr2, conditions = []) {
  return isEqual(sortBy(arr1, conditions), sortBy(arr2, conditions))
}

/**
 * Used to find out the target column definition for the given column name
 * @param {Array} columnDefs the given column definitions
 * @param {string} columnName the given column name
 */
export function findColDef(columnDefs, columnName) {
  if (isEmptyArray(columnDefs) || !isValidString(columnName)) return

  let columnDef = find(columnDefs, ['field', columnName])
  if (columnDef) return columnDef

  for (let i = 0; i < columnDefs.length; i++) {
    columnDef = findColDef(columnDefs[i].children, columnName)
    if (columnDef) return columnDef
  }
}

/**
 * Used to judge the give column definitions is grouped or not
 * @param {Array} columnDefs the given column definitions
 * @returns {boolean|boolean}
 */
export function isGroupedColumns(columnDefs) {
  return (
    !isEmptyArray(columnDefs) &&
    findIndex(columnDefs, (colItem) => has(colItem, 'children')) > -1
  )
}

/***
 * Check a string is valid date string or not
 * @param {string} str the given string
 * @returns {boolean}
 */
export const isValidDateStr = function (str) {
  return isValidString(str) && !isNaN(new Date(str).getTime())
}

/**
 * Format the date string
 * @param {string} value the given string to be format
 * @returns {string}
 */
export const formatDateStr = function (value) {
  if (!isValidDateStr(value)) return `${RVA_DEFAULT_DATE_MIN_YEAR}`

  let dateStr = value
  // remove time
  if (dateStr.indexOf(':') > 0) {
    const tempArr1 = dateStr.trim().split(/ |T/)
    const found = find(tempArr1, (item) => item.indexOf(':') < 0)
    if (found) dateStr = found
  }

  let tempArr = dateStr
    .replace(/\s/g, '')
    .split(/-| |,|\/|\\|\.|=|!|@|#|$|%|^|&|\*/)

  if (tempArr.length === 0) return `${RVA_DEFAULT_DATE_MIN_YEAR}`

  if (tempArr.length === 3) {
    tempArr = tempArr.map((item, index) => {
      if (index === 0 && item.length < 4) {
        // for year
        return RVA_DEFAULT_DATE_MIN_YEAR
      }
      return item
    })
  }
  if (
    (tempArr.length === 2 && tempArr[0].length < 4) ||
    (tempArr.length === 1 && tempArr[0].length < 4)
  ) {
    tempArr.unshift(RVA_DEFAULT_DATE_MIN_YEAR)
  }
  return tempArr.join('-')
}

/**
 * Append all cell renders into column definitions according to column types
 * @param columnDefs
 * @returns {*[]|*}
 */
export const appendCellRenders = function (columnDefs) {
  if (isEmptyArray(columnDefs)) return []

  return columnDefs.map((columnDef) => {
    if (columnDef && columnDef['type']) {
      if (isDesiredColType(columnDef['type'], RVA_COL_TYPES.DATE)) {
        columnDef['filter'] = 'agDateColumnFilter'
        columnDef['comparator'] = RVA_DEFAULT_DATE_COMPARATOR
        columnDef['valueFormatter'] = RVA_DEFAULT_DATE_FORMATTER
        columnDef['filterParams'] = RVA_DEFAULT_DATE_FILTER_PARAMS
      } else if (isDesiredColType(columnDef['type'], RVA_COL_TYPES.RANK)) {
        columnDef['width'] = 100
        columnDef['cellRendererFramework'] = 'RankCellRenderer'
      } else if (isDesiredColType(columnDef['type'], RVA_COL_TYPES.COUNTRY)) {
        columnDef['width'] = 200
        columnDef['minWidth'] = 200
        columnDef['maxWidth'] = 200
        columnDef['valueGetter'] = RVA_DEFAULT_COUNTRY_VALUE_GETTER
        columnDef['cellRendererFramework'] = 'CountryCellRenderer'
      } else if (isDesiredColType(columnDef['type'], RVA_COL_TYPES.PROGRESS)) {
        columnDef['cellStyle'] = {
          padding: 0,
          'white-space': 'normal',
        }
        columnDef['cellRendererFramework'] = 'ProgressCellRenderer'
        columnDef['pinnedRowCellRendererParams'] = {
          toPercentage: true,
        }
      } else if (isDesiredColType(columnDef['type'], RVA_COL_TYPES.LINKS)) {
        columnDef['cellStyle'] = {
          overflow: 'visible',
          'white-space': 'normal',
        }
        // TODO: needs to enhance in the future
        // columnDef['valueFormatter'] = RVA_DEFAULT_LINKS_FORMATTER
        // columnDef['cellRendererFramework'] = 'LinksCellRenderer'
      }
    }
    if (columnDef.children) {
      columnDef.children = appendCellRenders(columnDef.children)
    }
    return columnDef
  })
}

function isDesiredColType(sourceType, type) {
  return (
    (Array.isArray(sourceType) && sourceType.includes(type)) ||
    (isValidString(sourceType) && sourceType === type)
  )
}

export const getRouteDef = function(routes, route) {
  if (isEmpty(routes) || !isValidString(route.name)) return

  let result = find(routes, { name: route.name })
  if (result) return result

  for (let i = 0; i < routes.length; i++) {
    if (!isEmpty(routes[i].children)) {
      result = getRouteDef(routes[i].children, route)
      if (result) return result
    }
  }
}

export const fileToBase64 = function (file) {
  return new Promise((resolve, reject) => {
    if (file) {
      const reader = new FileReader()
      reader.onload = () => {
        const base64result = reader.result.split(',')[1]
        resolve(base64result)
      }
      reader.onerror = (error) => reject(error)
      if (file) {
        reader.readAsDataURL(file)
      }
    } else {
      reject('Invalid file.')
    }
  })
}

/**
 * Abbreviate the given number
 * @param num
 * @param digits
 * @returns {string}
 */
export const abbreviateNum = function (num, digits = 1) {
  const abbrev = ['', 'K', 'M', 'B', 't', 'q', 'Q'];
  const unrangifiedOrder = Math.floor(Math.log10(Math.abs(num)) / 3)
  const order = Math.max(0, Math.min(unrangifiedOrder, abbrev.length -1 ))
  const suffix = abbrev[order];
  return (num / Math.pow(10, order * 3)).toFixed(digits) + suffix;
}
