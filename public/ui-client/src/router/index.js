import Vue from 'vue'
import store from '@/store'
import VueRouter from 'vue-router'
import Home from '@/views/home/Index'
import * as SYS from '@/router/configs/system'
import * as ABOUT from '@/router/configs/about'
import { ROUTE_NAMES } from '@/router/configs/common'

Vue.use(VueRouter)

export const routes = [
  {
    path: '/',
    name: ROUTE_NAMES.HOME,
    component: Home,
    meta: {
      title: 'Home',
      icon: 'mdi-home',
      transition: 'fade-in-right',
    },
  },
  {
    path: '/login',
    name: ROUTE_NAMES.LOGIN,
    component: () => import('@/views/Login'),
    meta: {
      title: 'Login',
      requiresGuest: true,
      transition: 'fade-in-left',
    },
  },
  {
    path: '/explore',
    name: ROUTE_NAMES.EXPLORE,
    component: () => import('@/views/Index'),
    redirect: { name: ROUTE_NAMES.ABOUT },
    children: [
      {
        path: ROUTE_NAMES.ABOUT,
        name: ROUTE_NAMES.ABOUT,
        component: () => import('@/views/about/Index'),
        meta: ABOUT['ROOT_META'],
      },
      {
        path: ROUTE_NAMES.SYSTEM,
        name: ROUTE_NAMES.SYSTEM,
        component: () => import('@/views/system/Index'),
        meta: SYS['ROOT_META'],
      },
    ],
  },
  {
    path: '*',
    redirect: '/',
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return {
        selector: to.hash,
      }
    }
    return {
      x: 0,
      y: 0,
    }
  },
})

router.beforeEach((to, from, next) => {
  // set page title
  let title = to.meta && to.meta.title
  Vue.nextTick(() => {
    document.title =
      (title && `Revel Vuetify Admin | ${title}`) || 'Revel Vuetify Admin'
  })

  // user authentication
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    store
      .dispatch('user/checkLogin')
      .then(() => {
        next()
      })
      .catch((err) => {
        console.error(err)
        next({
          name: 'login',
          query: {
            redirect: to.fullPath,
          },
        })
      })
  } else if (to.matched.some((record) => record.meta.requiresGuest)) {
    store
      .dispatch('user/checkLogin')
      .then(() => {
        next({
          // path: '/',
          name: 'home',
          query: {
            redirect: to.fullPath,
          },
        })
      })
      .catch(() => {
        next()
      })
  } else {
    next()
  }
})

export default router
