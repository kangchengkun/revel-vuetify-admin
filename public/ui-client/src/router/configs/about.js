import { BREADCRUMB } from '@/router/configs/common'

export const ABOUT_SECTIONS = []

export const ABOUT_COMPONENTS = {}

const COMMON_META = {
  requiresAuth: true,
  mergeParentMeta: true,
  drawer: {
    mini: true,
  },
}

export const ROOT_META = {
  ...COMMON_META,
  requiresAuth: true,
  title: 'About Us',
  icon: 'mdi-information-variant',
  document: '', // RVA Manual.pdf
  breadcrumb: BREADCRUMB.HOME,
  description: 'Basic information for our project and team members.',
  tabs: [],
}
