export const ROUTE_NAMES = {
  HOME: 'home',
  LOGIN: 'login',
  ABOUT: 'about',
  SYSTEM: 'system',
  EXPLORE: 'explore',
}

export const BREADCRUMB = {
  HOME: {
    parent: 'home',
  },
}

