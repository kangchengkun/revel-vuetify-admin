import { BREADCRUMB } from '@/router/configs/common'

export const SYSTEM_SECTIONS = []

export const SYSTEM_COMPONENTS = {}

const COMMON_META = {
    requiresAuth: true,
    mergeParentMeta: true,
    drawer: {
        mini: true,
    },
}

export const ROOT_META = {
    ...COMMON_META,
    requiresAuth: true,
    title: 'Server Status',
    icon: 'mdi-cogs',
    document: '', // RVA Manual.pdf
    breadcrumb: BREADCRUMB.HOME,
    description: 'Basic information for the server you are using.',
    tabs: [],
}
