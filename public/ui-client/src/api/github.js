import axios from 'axios'

const GIT_HUB_CLIENT = {
  CLIENT_ID: '8d074bdfef4bd0e9ec37',
  CLIENT_SECRET: '805745c2c5b076c1e3ae097197b176678a743375',
  CLIENT_CODE: 'http://localhost:8080/?code=9fbbd2e98c1fe161596e',
}

const client = axios.create()

client.interceptors.request.use((config) => {
  return config
})

client.interceptors.response.use(
    (resp) => {
      return resp
    },
    (error) => {
      return error
    }
)

export function Commits(page) {
  return client({
    url:
      'https://api.github.com/repos/kangchengkun/revel-vuetify-admin/commits?page=' +
      page,
    method: 'get',
  })
}

export function Members() {
  return client({
    url: 'https://api.github.com/orgs/kck-00/members',
    method: 'get',
  })
}
