// mixin for using snackbar in page
import BaseSnackbar from '@/components/general/BaseSnackbar';

const SnackbarMixin = {
  components: {
    BaseSnackbar,
  },
  data() {
    return {
      snackbarColor: '',
      snackbarText: '',
    };
  },
  props: {},
  watch: {},
  computed: {},
  methods: {
    showMessage(msg, color) {
      if (!msg) return;
      this.snackbarColor = color || `info`;
      this.snackbarText = msg;
      setTimeout(() => {
        this.snackbarText = '';
      }, 5000);
    },
  },
  created() {},
  mounted() {},
  activated() {},
};

export default SnackbarMixin;
