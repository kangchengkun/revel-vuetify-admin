import has from 'lodash/has'

const fieldMixin = {
  props: {
    field: Object
  },
  computed: {
    label: function () {
      return `${this.field.label}${this.readonly ? ' - Readonly' : ''}`
    },
    itemText: function () {
      // item-text default 'text'
      return has(this.field, 'itemText') ? this.field.itemText : 'text'
    },
    itemValue: function () {
      // item-value default 'value'
      return has(this.field, 'itemValue') ? this.field.itemValue : 'value'
    },
    color: function () {
      return has(this.field, 'color') ? this.field.color : 'primary'
    },
    rules: function () {
      return has(this.field, 'rules') ? this.field.rules : []
    },
    dense: function () {
      return has(this.field, 'dense') ? this.field.dense : true
    },
    clearable: function () {
      // TODO: :clearable="!param.readonly", just like how the label is computed
      // return has(this.field, 'clearable') ? this.field.clearable : true
      return !this.readonly // TODO: test EditTask Page
    },
    readonly: function () {
      return has(this.field, 'readonly') ? this.field.readonly : false
    },
    outlined: function () {
      return has(this.field, 'outlined') ? this.field.outlined : true
    },
    disabled: function () {
      return has(this.field, 'disabled') ? this.field.disabled : false
    },
    classes: function () {
      // TODO: doc the acceptable format, seperate classes with blank space, fex, 'py0 mx-1'
      return has(this.field, 'classes') ? this.field.classes : ''
    },
    loading: function () {
      return has(this.field, 'loading') ? this.field.loading : false
    },
    linked: function () {
      return has(this.field, 'linked') ? this.field.linked : false
    },
    hideDetails: function () {
      return has(this.field, 'hideDetails') ? this.field.hideDetails : false
    },
    returnObject: function () {
      // Changes the selection behavior to return an array of selected items directly rather than the value specified with item-value
      return has(this.field, 'returnObject') ? this.field.returnObject : false
    }
  }
}

export default fieldMixin;