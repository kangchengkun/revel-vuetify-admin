import has from 'lodash/has'
import { AgGridVue } from 'ag-grid-vue'
import { RVA_DEFAULT_PAGE_SIZE, RVA_DEFAULT_COMPARATOR } from '@/utils/constants'

const agMixin = {
  components: {
    AgGridVue,
  },
  data() {
    return {
      maxChar: 200,
      gridOptions: null,
      gridApi: null,
      columnApi: null,
    }
  },
  methods: {
    /**
     * @param {Object=} args accept an object which may include the following fields:
     *
     * autoSizeColumns: boolean (If set to true, will call api.autoSizeColumns() to set the best width for each column to fit
     * the content. Otherwise, will call api.sizeColumnsToFit() to make the currently visible columns fit the available width.
     * Note: the width of columns with suppressSizeToFit=true is fixed.)
     * rowHeight: number, default is 25 (piexl)
     * autoRowHeight: boolean, Note: autoHeight would not work if either sizeColumnsToFit or autoSizeColumns set to true.
     * autoGridHeight: boolean
     * pagination: boolean, true by default
     * paginationPageSize: number
     * rowSelection: 'multiple' or 'single'
     * checkboxSelection: boolean
     * suppressRowClickSelection: boolean
     * rowDeselection: boolean
     * headerCheckboxSelectionFilteredOnly: boolean
     * noRowsText: string, text to display in the overlayNoRowsTemplate
     * @return {gridOptions}
     */
    getGridOptions(args) {
      return {
        defaultColDef: {
          width: args.width,
          minWidth: args.minWidth,
          autoHeight: args.autoHeight,
          resizable:
            typeof args.resizable !== 'undefined' ? args.resizable : true,
          sortable: typeof args.sortable !== 'undefined' ? args.sortable : true,
          filter: typeof args.filter !== 'undefined' ? args.filter : true,
          floatingFilter: args.floatingFilter || false,
          filterParams: {
            buttons: ['clear', 'reset'],
          },
          // Enable checkbox on the first visible column (if applicable)
          checkboxSelection: args.checkboxSelection
            ? this.enableCheckboxOnFirstColumn
            : false,

          // CSS class applies to all cells in the grid
          cellClass: args.cellClass,
          cellStyle: args.cellStyle,

          // Global cellRendererFramework applies to all the cells in the grid
          cellRendererFramework: args.cellRendererFramework,

          // Global valueFormatter applies to all the cells in the grid
          valueFormatter: args.valueFormatter,

          // Note: since v20.1, colDef.tooltip is gone, use tooltipValueGetter instead
          tooltip: args.tooltip,
          tooltipValueGetter: args.tooltipValueGetter,
          tooltipField: args.tooltipField,
          comparator: RVA_DEFAULT_COMPARATOR,

          // Global tooltipComponent applies to all cells in the grid
          tooltipComponent: args.tooltipComponent,

          // For multiple selection, add a checkbox in the header of the first visible column
          headerCheckboxSelection:
            args.rowSelection === 'multiple'
              ? this.enableCheckboxOnFirstColumn
              : false,
          // Configure header checkbox to select all rows or only filtered rows (true by default)
          headerCheckboxSelectionFilteredOnly: has(
            args,
            'headerCheckboxSelectionFilteredOnly'
          )
            ? args.headerCheckboxSelectionFilteredOnly
            : true,
        },
        context: {
          componentParent: this,
        },

        animateRows: true,
        tooltipShowDelay: 0,
        rowHeight: args.rowHeight,
        enableRangeSelection: args.enableRangeSelection,
        enableBrowserTooltips: args.enableBrowserTooltips,
        skipHeaderOnAutoSize: args.skipHeaderOnAutoSize,
        onColumnResized: args.autoRowHeight ? this.onColumnResized : null,

        // Set grid property domLayout='autoHeight' to allow the grid to auto-size it's height to fit rows
        // Note: when domLayout='autoHeight' then your application should not set height on the grid div
        domLayout: args.autoGridHeight ? 'autoHeight' : 'normal',

        // Enable sinlge or multiple row selection
        rowSelection: args.rowSelection,

        // Set to true to allow multiple rows to be selected with clicks
        rowMultiSelectWithClick:
          args.rowSelection === 'multiple' ? true : false,

        // Used only when checkbox selection not enabled, hold down Ctrl to deselect
        rowDeselection: args.rowDeselection,

        // If true, rows won't be selected when clicked; usually use with checkboxSelection
        suppressRowClickSelection: args.suppressRowClickSelection,

        ...((args.rowSelection === 'single' ||
          args.rowSelection === 'multiple') && {
          onSelectionChanged: this.onSelectionChanged,
        }),

        // Enable pagination
        pagination: args.pagination,
        paginationPageSize: args.paginationPageSize || RVA_DEFAULT_PAGE_SIZE,
        // If true, the grid will automatically show as many rows in each page as it can fit
        paginationAutoPageSize: !args.pagination
          ? false
          : args.paginationPageSize
          ? false
          : true,

        // For infinite ag grid
        rowBuffer: args.rowBuffer,
        rowModelType: args.rowModelType,
        cacheOverflowSize: args.cacheOverflowSize,
        maxConcurrentDatasourceRequests: args.maxConcurrentDatasourceRequests,
        infiniteInitialRowCount: args.infiniteInitialRowCount,
        maxBlocksInCache: args.maxBlocksInCache,

        // Size columns, disabled if autoRowHeight is set to true. Otherwise, call api.sizeColumnsToFit() by default
        // or call api.autoSizeColumns() if autoSizeColumns set to true
        onFirstDataRendered: !args.autoRowHeight
          ? args.autoSizeColumns
            ? this.autoSizeColumns
            : this.sizeColumnsToFit
          : null,
        onColumnEverythingChanged: !args.autoRowHeight
          ? args.autoSizeColumns
            ? this.autoSizeColumns
            : this.sizeColumnsToFit
          : null,

        // Render all columns for autoSizeColumns to apply on all columns (used with api.autoSizeColumns())
        suppressColumnVirtualisation: args.autoSizeColumns,

        // Gets displayed when loading has complete but no rows to show (i.e. rowData = []) style="position: absolute;bottom: 1.5em;"
        overlayNoRowsTemplate: `<span class="overlay grey--text text--darken-1">
            ${has(args, 'noRowsText') ? args.noRowsText : 'No Data To Show'}
          </span>`,

        // When the table is first initialised, the loading template is displayed if rowData is set to
        // null or undefined; when the api function setRowData is called, the loading panel is hidden.
        // overlayLoadingTemplate: `<div class="container" style="transform: translateY(50%);">
        //     <div class="row fill-height justify-center align-content-center">
        //       <div role="progressbar" style="height: 25px; width: 25px;" aria-valuemin="0" aria-valuemax="100" class="v-progress-circular v-progress-circular--indeterminate primary--text">
        //         <svg xmlns="http://www.w3.org/2000/svg" viewBox="21.73913043478261 21.73913043478261 43.47826086956522 43.47826086956522" style="transform: rotate(0deg);">
        //           <circle fill="transparent" cx="43.47826086956522" cy="43.47826086956522" r="20" stroke-width="3.4782608695652177" stroke-dasharray="125.664" stroke-dashoffset="125.66370614359172px" class="v-progress-circular__overlay"></circle>
        //         </svg>
        //         <div class="v-progress-circular__info"></div>
        //       </div>
        //     </div
        //   ></div>`,
        sideBar: args.sideBar
          ? {
              toolPanels: [
                {
                  id: 'columns',
                  labelDefault: 'Columns',
                  labelKey: 'columns',
                  iconKey: 'columns',
                  toolPanel: 'agColumnsToolPanel',
                  toolPanelParams: {
                    suppressPivots: true,
                    suppressPivotMode: true,
                    suppressValues: true,
                    suppressRowGroups: true,
                  },
                },
                {
                  id: 'filters',
                  labelDefault: 'Filters',
                  labelKey: 'filters',
                  iconKey: 'filter',
                  toolPanel: 'agFiltersToolPanel',
                },
              ],
            }
          : null,
        statusBar: args.statusBar
          ? {
              statusPanels: [
                { statusPanel: 'agTotalRowCountComponent', align: 'left' },
                { statusPanel: 'agFilteredRowCountComponent' },
                { statusPanel: 'agSelectedRowCountComponent' },
                { statusPanel: 'agAggregationComponent' },
              ],
            }
          : null,
      }
    },
    setColumnVisible(field, isVisible) {
      this.gridColumnApi.setColumnVisible(field, isVisible)
    },
    // Make visible columns (except suppressSizeToFit=true) fit the screen
    sizeColumnsToFit(params) {
      params.api.sizeColumnsToFit()
    },
    // Set the best width to fit the contents of the cells in all columns (except suppressSizeToFit=true)
    autoSizeColumns(params) {
      const allColumnIds = params.columnApi.getAllColumns().map((col) => {
        // skip the columns with colDef.suppressSizeToFit set to true
        if (!col.colDef.suppressSizeToFit) {
          return col.colId
        }
      })
      params.columnApi.autoSizeColumns(allColumnIds)
    },
    getAllDisplayedColumns(params) {
      return params.columnApi.getAllDisplayedColumns()
    },
    pinFirstVisibleColumn() {
      const displayedColumns = this.gridColumnApi.getAllDisplayedColumns()
      if (displayedColumns.length > 0) {
        const firstColId = displayedColumns[0].colId
        this.gridColumnApi.setColumnPinned(firstColId, 'left')
      }
    },
    // Enable checkbox on the first visible column
    enableCheckboxOnFirstColumn(params) {
      const displayedColumns = params.columnApi.getAllDisplayedColumns()
      return params.column === displayedColumns[0]
    },
    // When the data changes or the width of a column changes, force the grid to re-calculate the row height
    onColumnResized(params) {
      params.api.resetRowHeights()
    },
    // Return a copy of rowData
    copyRowData(rowData) {
      if (!rowData) return null
      return rowData.map((row) => {
        return {
          ...row,
        }
      })
    },
    longTextFormatter(params) {
      if (typeof params.value !== 'string') {
        return params.value
      }

      let maxChar = params.colDef.autoHeight ? this.maxChar + 30 : this.maxChar
      let formattedValue =
        params.value.length > maxChar
          ? params.value.substring(0, maxChar)
          : params.value

      // avoid last word from being cut off
      formattedValue =
        formattedValue.length === maxChar
          ? formattedValue.substring(
              0,
              Math.min(formattedValue.length, formattedValue.lastIndexOf(' '))
            ) + ' ...'
          : formattedValue
      return formattedValue
    },
  },
  mounted() {
    // console.log('on mounted,this.gridOptions: ', this.gridOptions.api)
    this.gridApi = this.gridOptions.api
    this.gridColumnApi = this.gridOptions.columnApi
  },
}

export default agMixin
