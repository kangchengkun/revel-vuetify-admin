import { AgGridVue } from 'ag-grid-vue'

const agGridMixin = {
  components: {
    AgGridVue
  },
  data() {
    return {
      gridApi: null,
      columnApi: null
    }
  },
  mounted() {
    this.gridApi = this.gridOptions.api
    this.columnApi = this.gridOptions.columnApi
  },
  methods: {
    setColumnVisible(field, isVisible) {
      this.columnApi.setColumnVisible(field, isVisible)
    }
  }
}

export default agGridMixin
