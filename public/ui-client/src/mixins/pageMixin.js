import Alert from '@/components/general/notifs/Alert'
import BaseLoader from '@/components/general/BaseLoader'
import PageHeader from '@/components/general/page-wrapper/PageHeader'
import PageWrapper from '@/components/general/page-wrapper/Index'
import BaseRenderCard from '@/components/general/BaseRenderCard'
import SuccessOverlay from '@/components/general/notifs/SuccessOverlay'

const pageMixin = {
  components: {
    Alert,
    BaseLoader,
    PageHeader,
    PageWrapper,
    BaseRenderCard,
    SuccessOverlay
  },
  data() {
    return {
      alert: null,
      loading: false,
      isSuccess: false
    }
  },
  methods: {
    onFailure(message) {
      this.loading = false
      this.alert = { type: 'error', message }
    },
    onSuccess(message) {
      this.loading = false
      this.alert = { type: 'success', message }
    },
    onWarning(message) {
      this.loading = false
      this.alert = { type: 'warning', message }
    },
    dismissAlert() {
      this.alert = null
    },
    toggleLoading() {
      this.loading = !this.loading
    },
    setLoading() {
      this.loading = true
    },
    unsetLoading() {
      this.loading = false
    }
  }
}

export default pageMixin
