import has from 'lodash/has'
import find from 'lodash/find'
import isArray from 'lodash/isArray'
import { isEmptyArray } from '@/utils'

const formMixin = {
  methods: {
    setValue(fields, name, value) {
      const field = find(fields, { name: name })
      if (field) {
        if (this.isMultiSelection(field.type)) {
          const tempValue = isArray(value) ? value : [value]
          this.$set(field, 'value', tempValue)
          if (field.type === 'TaggedMultiSelect') {
            tempValue.forEach((valueEle) => {
              let item = find(field.items, { text: valueEle.text })
              this.$set(item, 'tag', valueEle.tag)
            })
          }
        } else {
          if (!isEmptyArray(value)) {
            this.$set(field, 'value', value[0])
          } else {
            this.$set(field, 'value', value)
          }
        }
      }
    },
    setOptions(fields, name, data) {
      const field = find(fields, { name: name })
      if (field) {
        if (!isArray(data)) {
          const items = []
          items.push(this.constructOption(data))
          this.$set(field, 'items', items)
        } else {
          this.$set(
            field,
            'items',
            data.map((item) => this.constructOption(item))
          )
        }
      }
    },
    addOption(field, data) {
      if (!has(field, 'items')) {
        const items = []
        items.push(this.constructOption(data))
        this.$set(field, 'items', items)
      } else {
        field.items.push(this.constructOption(data))
        console.log('after addOption: ', field)
      }
    },
    clearOptions(field) {
      if (field) this.$set(field, 'items', [])
    },
    setValueOption(fields, name, value) {
      this.setValue(fields, name, value)
      this.setOptions(fields, name, value)
    },
    clearValue(field) {
      if (field) {
        if (this.isMultiSelection(field.type)) {
          this.$set(field, 'value', [])
        } else {
          this.$set(field, 'value', '')
        }
      }
    },
    isMultiSelection(type) {
      return type === 'MultiSelect' || type === 'TaggedMultiSelect'
    },
    toggleFieldLoading(field) {
      if (field) {
        const loading = !field.loading
        this.$set(field, 'loading', loading)
        if (loading) {
          this.controlOptionsDisabled(field, true)
        } else {
          this.controlOptionsDisabled(field, false)
        }
      }
    },
    controlOptionsDisabled(field, disabled) {
      if (!field) return

      if (isArray(field.items)) {
        this.$set(
          field,
          'items',
          field.items.map((item) => {
            if (item && typeof item === 'object') {
              item.disabled = disabled
            }
            return item
          })
        )
      }
    },
    setFieldProp(field, prop, value) {
      if (field) this.$set(field, prop, value)
    },
    constructOption(optionValue) {
      return {
        text: optionValue,
        value: optionValue,
        disabled: false,
        divider: false,
        header: '',
      }
    },
  },
}

export default formMixin
