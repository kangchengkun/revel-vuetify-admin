import has from 'lodash/has';
import { AgGridVue } from 'ag-grid-vue';
import { isFinite } from 'lodash/isFinite';
import toNumber from 'lodash/toNumber';
import { RVA_DEFAULT_COMPARATOR } from '@/utils/constants';

const agEditMixin = {
  components: {
    AgGridVue,
  },
  data() {
    return {
      gridOptions: null,
      gridApi: null,
      columnApi: null,
    };
  },

  methods: {
    /**
     * @param {Object=} args accept an object which may include the following fields:
     *
     * autoSizeColumns: boolean (If set to true, will call api.autoSizeColumns() to set the best width for each column to fit
     * the content. Otherwise, will call api.sizeColumnsToFit() to make the currently visible columns fit the available width.
     * Note: the width of columns with suppressSizeToFit=true is fixed.)
     * rowHeight: number, default is 25 (piexl)
     * autoRowHeight: boolean, Note: autoHeight would not work if either sizeColumnsToFit or autoSizeColumns set to true.
     * autoGridHeight: boolean
     * pagination: boolean, true by default
     * paginationPageSize: number
     * rowSelection: 'multiple' or 'single'
     * checkboxSelection: boolean
     * suppressRowClickSelection: boolean
     * rowDeselection: boolean
     * headerCheckboxSelectionFilteredOnly: boolean
     * noRowsText: string, text to display in the overlayNoRowsTemplate
     * @return {gridOptions}
     */

    getGridOptions(args) {
      return {
        defaultColDef: {
          floatingFilter: false,
          //   width: 150,
          //   editable: true,
          //   resizable: (typeof args.resizable !== 'undefined') ? args.resizable : true,
          //   sortable: true,
          //   filter: true,
          //   valueParser: this.numberParser,

          comparator: RVA_DEFAULT_COMPARATOR,
          //   filterParams: {
          //     buttons: ['clear', 'reset'],
          //   },
          //   // Enable checkbox on the first visible column (if applicable)
          //   checkboxSelection: (args.checkboxSelection) ? this.enableCheckboxOnFirstColumn : false,

          //   // CSS class applies to all cells in the grid
          //   cellClass: args.cellClass,

          //   // Global cellRendererFramework applies to all the cells in the grid
          //   cellRendererFramework: args.cellRendererFramework,

          //   // Global valueFormatter applies to all the cells in the grid
          //   valueFormatter: args.valueFormatter,

          //   // Note: since v20.1, colDef.tooltip is gone, use tooltipValueGetter instead
          //   tooltip: args.tooltip,
          //   tooltipValueGetter: args.tooltipValueGetter,
          //   tooltipField: args.tooltipField,

          //   // Global tooltipComponent applies to all cells in the grid
          //   tooltipComponent: args.tooltipComponent,

          //   // For multiple selection, add a checkbox in the header of the first visible column
          //   headerCheckboxSelection: (args.rowSelection === 'multiple') ? this.enableCheckboxOnFirstColumn : false,
          //   // Configure header checkbox to select all rows or only filtered rows (true by default)
          //   headerCheckboxSelectionFilteredOnly: (has(args, 'headerCheckboxSelectionFilteredOnly')) ? args.headerCheckboxSelectionFilteredOnly : true,
        },

        context: {
          componentParent: this,
        },

        enableBrowserTooltips: args.enableBrowserTooltips,

        animateRows: true,

        rowHeight: args.rowHeight,

        onColumnResized: args.autoRowHeight ? this.onColumnResized : null,

        // Set grid property domLayout='autoHeight' to allow the grid to auto-size it's height to fit rows
        // Note: when domLayout='autoHeight' then your application should not set height on the grid div
        domLayout: args.autoGridHeight ? 'autoHeight' : '',

        // Enable sinlge or multiple row selection
        rowSelection: args.rowSelection,

        // Set to true to allow multiple rows to be selected with clicks
        rowMultiSelectWithClick:
          args.rowSelection === 'multiple' ? true : false,

        // Used only when checkbox selection not enabled, hold down Ctrl to deselect
        rowDeselection: args.rowDeselection,

        // If true, rows won't be selected when clicked; usually use with checkboxSelection
        suppressRowClickSelection: args.suppressRowClickSelection,

        // Enable pagination
        pagination: args.pagination,
        paginationPageSize: args.paginationPageSize,
        // If true, the grid will automatically show as many rows in each page as it can fit
        paginationAutoPageSize: !args.pagination
          ? false
          : args.paginationPageSize
          ? false
          : true,

        // Size columns, disabled if autoRowHeight is set to true. Otherwisee, call api.sizeColumnsToFit() by default
        // or call api.autoSizeColumns() if autoSizeColumns set to true
        onFirstDataRendered: !args.autoRowHeight
          ? args.autoSizeColumns
            ? this.autoSizeColumns
            : this.sizeColumnsToFit
          : null,

        // Render all columns for autoSizeColumns to apply on all columns (used with api.autoSizeColumns())
        suppressColumnVirtualisation: args.autoSizeColumns,

        // Gets displayed when loading has complete but no rows to show (i.e. rowData = [])
        overlayNoRowsTemplate: `<span class="overlay grey--text text--darken-1" style="transform: translateY(70%);position: absolute;bottom: 1.5em;">
            ${has(args, 'noRowsText') ? args.noRowsText : 'No Data To Show'}
          </span>`,

        // When the table is first initialised, the loading template is displayed if rowData is set to
        // null or undefined; when the api function setRowData is called, the loading panel is hidden.
        overlayLoadingTemplate: `<div class="container" style="transform: translateY(50%);">
            <div class="row fill-height justify-center align-content-center">
              <div role="progressbar" style="height: 25px; width: 25px;" aria-valuemin="0" aria-valuemax="100" class="v-progress-circular v-progress-circular--indeterminate primary--text">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="21.73913043478261 21.73913043478261 43.47826086956522 43.47826086956522" style="transform: rotate(0deg);">
                  <circle fill="transparent" cx="43.47826086956522" cy="43.47826086956522" r="20" stroke-width="3.4782608695652177" stroke-dasharray="125.664" stroke-dashoffset="125.66370614359172px" class="v-progress-circular__overlay"></circle>
                </svg>
                <div class="v-progress-circular__info"></div>
              </div>
            </div
          ></div>`,
      };
    },
    // Make visible columns (except suppressSizeToFit=true) fit the screen
    sizeColumnsToFit(params) {
      params.api.sizeColumnsToFit();
    },
    // Set the best width to fit the contents of the cells in all columns (except suppressSizeToFit=true)
    autoSizeColumns(params) {
      const allColumnIds = params.columnApi.getAllColumns().map(col => {
        // skip the columns with colDef.suppressSizeToFit set to true
        if (!col.colDef.suppressSizeToFit) {
          return col.colId;
        }
      });
      params.columnApi.autoSizeColumns(allColumnIds);
    },
    // Enable checkbox on the first visible column
    enableCheckboxOnFirstColumn(params) {
      const displayedColumns = params.columnApi.getAllDisplayedColumns();
      return params.column === displayedColumns[0];
    },
    // When the data changes or the width of a column changes, force the grid to re-calculate the row height
    onColumnResized() {
      this.gridApi.resetRowHeights();
    },

    // Return a copy of rowData
    copyRowData(rowData) {
      if (!rowData) return null;
      return rowData.map(row => {
        return {
          ...row,
        };
      });
    },
    numberParser(params) {
      if (!isFinite(toNumber(params.newValue))) {
        return false;
      }
      params.data.Epidemiology = toNumber(params.newValue);
      return true;
    },

    onBtWhich() {
      var cellDefs = this.gridApi.getEditingCells();
      if (cellDefs.length > 0) {
        var cellDef = cellDefs[0];
        console.log(
          'editing cell is: row = ' +
            cellDef.rowIndex +
            ', col = ' +
            cellDef.column.getId() +
            ', floating = ' +
            cellDef.rowPinned
        );
      } else {
        console.log('no cells are editing');
      }
    },
  },
  mounted() {
    this.gridApi = this.gridOptions.api;
    this.gridColumnApi = this.gridOptions.columnApi;
  },
};

export default agEditMixin;
