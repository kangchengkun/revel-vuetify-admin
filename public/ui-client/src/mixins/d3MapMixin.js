// mixin for components on d3 map
import * as d3 from 'd3'
import { generateRandomKey } from '@/utils/index.js'
import D3MapZoom from '@/components/general/D3MapZoom'
import BaseD3MapTooltip from '@/components/general/BaseD3MapTooltip'

const D3MapMixin = {
  components: {
    D3MapZoom,
    BaseD3MapTooltip,
  },
  props: {
    geoData: {
      type: Object,
      default() {
        return null
      },
    },
    mapData: {
      type: Object,
      default() {
        return null
      },
    },
  },
  data() {
    return {
      mapWidth: 970,
      mapHeight: 620,
      mapSvg: null,
      zoomLevel: 1,
      zoomDelt: 1.2,
      d3Zoom: d3.zoom(),
      tooltipTexts: [],
      tooltipCharts: [],
      legendHeight: 25,
      legendWidth: 700,
      legendId: `legend-${generateRandomKey()}`,
      mapContainerId: `map-container-${generateRandomKey()}`,
      mapToolTipContainerId: `map-tooltip-container-${generateRandomKey()}`,
      legendMargin: { top: 15, right: 20, bottom: 10, left: 20 },
    }
  },
  watch: {},
  computed: {},
  created() {},
  mounted() {},
  methods: {
    mapZoomIn() {
      if (this.mapSvg) {
        this.d3Zoom.scaleTo(this.mapSvg, this.zoomLevel * this.zoomDelt)
      }
    },
    mapZoomOut() {
      if (this.mapSvg) {
        this.d3Zoom.scaleTo(this.mapSvg, this.zoomLevel / this.zoomDelt)
      }
    },
    mapZoomReset() {
      if (this.mapSvg) {
        this.clearMapTooltipData()
        this.d3Zoom.transform(
          this.mapSvg,
          d3['zoomIdentity'].translate(0, 0).scale(1)
        )
      }
    },
    clearMapTooltipData() {
      this.tooltipTexts = []
      this.tooltipCharts = []
    },
  },
  activated() {},
}

export default D3MapMixin
