import Alert from '@/components/general/notifs/Alert'
import FilterMenu from '@/components/general/menus/FilterMenu'

const sectionMixin = {
  components: {
    Alert,
    FilterMenu
  },
  data () {
    return {
      alert: null
    }
  },
  methods: {
    dismissAlert () {
      this.alert = null
    },
    onSuccess (message) {
      this.alert = { type: 'info', message }
    },
    onFailure (message) {
      this.alert = { type: 'error', message }
    }
  }
}

export default sectionMixin