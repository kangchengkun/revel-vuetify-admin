import './filters'
import 'ag-grid-enterprise'
import './registerServiceWorker'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from '@/i18n/vue-i18n/'
import vuetify from './plugins/vuetify'
import Particles from 'particles.vue'
import VueScrollactive from 'vue-scrollactive'
import VuePageTransition from 'vue-page-transition'

import v1 from 'uuid/v1'
import v3 from 'uuid/v3'
import v4 from 'uuid/v4'
import v5 from 'uuid/v5'

/**
 * An object with uuid's v1, v3, v4 and v5 functions
 * @type {UUID}
 */
export const uuid = { v1, v3, v4, v5 }

Vue.config.productionTip = false

Vue.prototype.bus = new Vue()
Vue.prototype.$uuid = uuid
Vue.prototype.$hostname = process.env.VUE_APP_ENVOY_ADDR

Vue.use(Particles)
Vue.use(VueScrollactive)
Vue.use(VuePageTransition)

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
