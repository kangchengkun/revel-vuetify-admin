import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import geo from './modules/geo'
import user from './modules/user'
import drawer from './modules/drawer'
import breadcrumb from './modules/breadcrumb'
import countryCodes from './modules/country-codes'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  reducer: state => ({
    drawer: {
      drawer: state.drawer.menuType,
    },
  }),
})

export default new Vuex.Store({
  namespaced: true,
  modules: {
    geo,
    user,
    drawer,
    breadcrumb,
    countryCodes,
  },
  plugins: [vuexLocal.plugin],
})
