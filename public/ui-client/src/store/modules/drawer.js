import { splitCamelCase } from '@/utils'
import { DEFAULT_MENU } from '@/components/general/nav-drawer/config'

const state = {
  menuType: DEFAULT_MENU
}

const getters = {
  menuTitle: state => {
    return splitCamelCase(state.menuType)
  }
}

const mutations = {
  setMenuType: (state, type) => {
    state.menuType = type || DEFAULT_MENU
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}