import axios from 'axios'

const state = {
  geoData: null
}

const getters = {}

const actions = {
  getGeoData: ({ commit }) => {
    return new Promise((resolve, reject) => {
      axios({
        url: '/countries/rough.geo.json',
        baseURL: process.env.VUE_APP_FILE_SERVER
      })
        .then(({ data }) => {
          commit('setGeoData', data)
          resolve(data)
        })
        .catch(error => {
          console.error(error.message)
          reject('Failed to read geo json from file server')
        })
    })
  }
}

const mutations = {
  setGeoData: (state, data) => {
    state.geoData = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}