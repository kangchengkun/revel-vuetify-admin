// The paths for vuex store
export const STORE_PATHS = {
  USER: 'user',
  SITE: 'site',
  TASKS: 'tasks',
  STUDIES: 'studies',
  COUNTRY: 'country',
  BREADCRUMB: 'breadcrumb',
};
