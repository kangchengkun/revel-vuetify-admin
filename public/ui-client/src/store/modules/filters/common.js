import Vue from 'vue'
import { omitEmpty } from '@/utils'
import { has, find, cloneDeep, isEmpty } from 'lodash'
import { baseClient } from '@/service/http-client/grpc'

export const F_STATE = {
  FORM_FIELDS: 'formFields'
}

export const F_GETTERS = {
  FORM_FIELDS: 'formFields',
  FORM_DATA: 'formData',
  FIELD: 'field',
  FIELD_VALUE: 'fieldValue',
  FIELD_OPTIONS: 'fieldOptions',
  IS_LINKED_FIELD: 'isLinkedField',
  LINKED_FIELDS: 'linkedFields',
  LINKED_UPSTREAM_FIELDS: 'upstreamFields',
  LINKED_DOWNSTREAM_FIELDS: 'downstreamFields',
  LINKED_FIELDS_DATA: 'linkedFieldsData'
}

export const F_ACTIONS = {
  INIT_FORM_FIELDS: 'initFormFields',
  INIT_FIELDS_OPTIONS: 'initFieldsOptions',
  GET_FIELD_OPTIONS: 'getFieldOptions',
  GET_INTERNAL_FIELD_OPTIONS: 'getInternalFieldOptions',
  GET_LINKED_FIELD_OPTIONS: 'getLinkedFieldOptions',
  UPDATE_LINKED_FIELDS: 'updateLinkedFields'
}

export const F_MUTATIONS = {
  SET_FORM_FIELDS: 'setFormFields',
  SET_FIELD_VALUE: 'setFieldValue',
  SET_FIELD_OPTIONS: 'setFieldOptions',
  APPEND_FIELD_OPTIONS: 'appendFieldOptions',
  CLEAR_FIELD_VALUE: 'clearFieldValue',
  CLEAR_FIELD_OPTIONS: 'clearFieldOptions',
  TOGGLE_FIELD_LOADING: 'toggleFieldLoading'
}

export const commonState = {
  [F_STATE.FORM_FIELDS]: []
}

export const commonGetters = {
  // remove
  [F_GETTERS.FORM_FIELDS]: state => {
    return state.formFields
  },
  [F_GETTERS.FORM_DATA]: (state) => {
    return state.formFields.reduce((acc, field) => {
      if (typeof field.value !== 'undefined') {
        switch (field.type) {
          case 'TaggedMultiSelect':
            field.value.forEach((item) => {
              const name = `${field.name}_${item.tag}`
              if (has(acc, name)) {
                acc[name].push(item.text)
              } else {
                acc[name] = [item.text]
              }
            })
            break
          case 'DateRangeCombo' || 'NumberRangeCombo' || 'MultiSelectCombo':
            acc = { ...acc, ...field.value }
            break
          default:
            acc[field.name] = field.value
            break
        }
      }
      return acc
    }, {})
  },
  [F_GETTERS.LINKED_FIELDS]: state => {
    return state.formFields.reduce(
      (acc, field) => {
        if (has(field, 'linked') && field.linked) {
          acc.push(field)
        }
        return acc
      }, [])
  },
  [F_GETTERS.FIELD]: state => name => {
    return find(state.formFields, ['name', name])
  },
  [F_GETTERS.IS_LINKED_FIELD]: (state, getters) => name => {
    const field = getters.field(name)
    return (field && field.linked) || false
  },
  [F_GETTERS.LINKED_UPSTREAM_FIELDS]: (state, getters) => name => {
    const field = getters.field(name)
    if (field.linked) {
      const dependency = field.dependency
      const linkedFields = getters.linkedFields
      return linkedFields.filter(field => {
        return field.dependency < dependency
      })
    }
    return []
  },
  [F_GETTERS.LINKED_DOWNSTREAM_FIELDS]: (state, getters) => name => {
    const field = getters.field(name)
    if (field.linked) {
      const dependency = field.dependency
      return getters.linkedFields.filter(field => {
        return field.dependency > dependency
      })
    }
    return []
  },
  [F_GETTERS.LINKED_FIELDS_DATA]: state => {
    return state.formFields.reduce(
      (acc, field) => {
        if (has(field, 'linked') && field.linked && field.value) {
          let value = field.value
          if (field.acceptsArray && !Array.isArray(field.value)) {
            // convert submission value to array
            value = [value]
          }
          acc[field.name] = value
        }
        return acc
      }, {})
  },
  [F_GETTERS.FIELD_VALUE]: (state, getters) => name => {
    const field = getters.field(name)
    return field && field.value || ''
  },
  [F_GETTERS.FIELD_OPTIONS]: (state, getters) => name => {
    const field = getters.field(name)
    return field && field.items || []
  }
}

export const commonActions = {
  // Retrieve field options from get_int_per_options API
  [F_ACTIONS.GET_INTERNAL_FIELD_OPTIONS]: ({ commit, getters }, name) => {
    return new Promise((resolve, reject) => {
      commit('clearFieldOptions', name)
      commit('toggleFieldLoading', name)

      const params = omitEmpty(getters.linkedFieldsData)
      const payload = {
        target: name,
        ...params
      }
      // omit the target field value
      delete payload[name]

      // baseClient.post('/internal/options', payload)
      //   .then(({ data }) => {
      //     if (data.code !== 0) {
      //       throw new Error(data.message)
      //     }
      //     commit('setFieldOptions', { name, options: data.data })
      //     resolve(data.data)
      //   })
      //   .catch(e => {
      //     console.error(e.message)
      //     reject(e)
      //   })
      //   .then(commit('toggleFieldLoading', name))

      baseClient
        .post(`/v1/int-perf-opts`, payload)
        .then(({ data }) => {
          const options = data['item_content']
          commit('setFieldOptions', { name, options })
          resolve(options)
        })
        .catch((e) => {
          console.error(e)
          reject(
            `Failed to fetch options for ${name}: ${JSON.stringify(e)}`
          )
        })
        .then(commit('toggleFieldLoading', name))
    })
  }
}

export const commonMutations = {
  [F_MUTATIONS.SET_FORM_FIELDS]: (state, data) => {
    state.formFields = cloneDeep(data)
  },
  [F_MUTATIONS.SET_FIELD_VALUE]: (state, { name, value }) => {
    const field = find(state.formFields, ['name', name])
    try {
      if (typeof field === 'undefined') {
        throw new Error(`Cannot set property 'value' of undefined field '${name}'`)
      }
      Vue.set(field, 'value', value)
    } catch (e) {
      console.error(e.message)
    }
  },
  [F_MUTATIONS.CLEAR_FIELD_VALUE]: (state, name) => {
    const field = find(state.formFields, ['name', name])
    try {
      if (typeof field === 'undefined') {
        throw new Error(`Cannot reset property 'value' of undefined field '${name}'`)
      }
      Vue.set(field, 'value', '')
    } catch (e) {
      console.error(e.message)
    }
  },
  [F_MUTATIONS.SET_FIELD_OPTIONS]: (state, { name, options }) => {
    const field = find(state.formFields, ['name', name])
    try {
      if (typeof field === 'undefined') {
        throw new Error(`Cannot set property 'items' of undefined field '${name}'`)
      }
      Vue.set(field, 'items', options)
    } catch (e) {
      console.error(e.message)
    }
  },
  [F_MUTATIONS.CLEAR_FIELD_OPTIONS]: (state, name) => {
    const field = find(state.formFields, ['name', name])
    try {
      if (typeof field === 'undefined') {
        throw new Error(`Cannot reset property 'items' of undefined field '${name}'`)
      }
      Vue.set(field, 'items', [])
    } catch (e) {
      console.error(e.message)
    }
  },
  // The item can either be a single item or an array of items
  [F_MUTATIONS.APPEND_FIELD_OPTIONS]: (state, { name, item }) => {
    if (isEmpty(item)) return
    const field = find(state.formFields, ['name', name])
    try {
      if (typeof field === 'undefined') {
        throw new Error(`Cannot add 'option' to a undefined field '${name}'`)
      }
      if (!Array.isArray(item)) {
        Vue.set(field, 'items', [...(field.items || []), item])
      } else {
        Vue.set(field, 'items', [...(field.items || []), ...item])
      }
    } catch (e) {
      console.error(e.message)
    }
  },
  [F_MUTATIONS.TOGGLE_FIELD_LOADING]: (state, name) => {
    const field = find(state.formFields, ['name', name])
    try {
      if (typeof field === 'undefined') {
        throw new Error(`Cannot set property 'loading' of undefined field '${name}'`)
      }
      Vue.set(field, 'loading', !field.loading)
    } catch (e) {
      console.error(e.message)
    }
  }
}
