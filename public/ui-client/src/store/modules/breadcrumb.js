const state = {
  breadcrumbs: []
}

const getters = {
  breadcrumbs: state => state.breadcrumbs
}

const mutations = {
  set: (state, items) => {
    state.breadcrumbs = items
  },
  push: (state, item) => {
    state.breadcrumbs.push(item)
  },
  pop: (state) => {
    state.breadcrumbs.pop()
  },
  replace: (state, target) => {
    const index = state.breadcrumbs.findIndex((breadcrumb) => {
      return breadcrumb.text === target.find
    })
    if (index) {
      state.breadcrumbs.splice(index, 1, target.replace)
    }
  },
  empty: (state) => {
    state.breadcrumbs = []
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}