import { baseClient } from '@/service/http-client/grpc'

const state = {
  user: {},
  csrfToken: ''
}

const getters = {
  user: (state, getters) => {
    return {
      id: getters.userId,
      name: getters.userName,
    }
  },
  userId: state => {
    return state.user ? state.user.Identity : '';
  },
  userName: state => {
    return state.user ? state.user.Name : '';
  }
}

const actions = {
  checkLogin: ({ commit }) => {
    return new Promise((resolve, reject) => {
      baseClient.get('/login_check')
        .then(({ data }) => {
          if (data.success) {
            commit('setUser', data.user)
            resolve(data.user)
          } else {
            throw new Error('not logged in')
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  login: ({ commit }, payload) => {
    return new Promise((resolve, reject) => {
      baseClient.post('/login', payload)
        .then(({ data }) => {
          if (data.success) {
            commit('setUser', data.user)
            resolve()
          } else {
            throw new Error(data.error)
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  logout: ({ commit }) => {
    return new Promise((resolve, reject) => {
      baseClient.get('/logout')
        .then(() => {
          commit('deleteUser')
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  getCSRFToken: ({ commit }) => {
    return new Promise((resolve, reject) => {
      baseClient.get('/get_token')
        .then(({ data }) => {
          commit('setCSRFToken', data.csrfToken)
          resolve(data.csrfToken)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

const mutations = {
  setUser: (state, user) => {
    state.user = user
  },
  deleteUser: (state) => {
    state.user = {}
  },
  setCSRFToken: (state, token) => {
    state.csrfToken = token
    // TODO: axios.defaults.headers.common['X-CSRF-TOKEN'] = token
    console.log('setCSRFToken: ', state.csrfToken)
    // console.log('axios.defaults.headers', axios.defaults.headers)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};