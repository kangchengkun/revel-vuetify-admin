// Main used to download the country codes and used for anywhere needed in the future
// For update the latest codes we need to provide a py script or manually download from ISO website
// https://datahub.io/core/country-codes is a reference which can download the codes
// https://github.com/datasets/country-codes provides some scripts can be used to download country codes.


import axios from 'axios'

export const COUNTRY_CODES_STATE = {
  COUNTRY_CODES: 'countryCodes',
}

export const COUNTRY_CODES_MUTATIONS = {
  SET_COUNTRY_CODES: 'setCountryCodes',
}

export const COUNTRY_CODES_ACTIONS = {
  GET_COUNTRY_CODES: 'getCountryCodes',
}

const state = {
  [COUNTRY_CODES_STATE.COUNTRY_CODES]: [],
}

const getters = {}

const actions = {
  [COUNTRY_CODES_ACTIONS.GET_COUNTRY_CODES]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      axios({
        url: '/countries/codes.json',
        baseURL: process.env.VUE_APP_FILE_SERVER,
      })
        .then(({ data }) => {
          commit(COUNTRY_CODES_MUTATIONS.SET_COUNTRY_CODES, data)
          resolve(data)
        })
        .catch((error) => {
          reject(
            `Failed to fetch country codes from file server with error: ${error}`
          )
        })
    })
  },
}

const mutations = {
  [COUNTRY_CODES_MUTATIONS.SET_COUNTRY_CODES]: (state, data) => {
    state[COUNTRY_CODES_STATE.COUNTRY_CODES] = data
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
