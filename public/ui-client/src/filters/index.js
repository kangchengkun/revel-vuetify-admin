import Vue from 'vue'

// Format a string to an id used for hash tag links
Vue.filter('formatId', function(value) {
  if (!value) return ''
  value = value.replace(/[^A-Za-z0-9]/g, ' ').replace(/  +/g, ' ')
  value = value.toLowerCase()
  return value.split(' ').join('-')
})

// Format an integer with commas as thousands separators
Vue.filter('formatNumber', function(value) {
  if (!value) return value
  if (typeof value === 'number') {
    value = value.toString()
  }
  return value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
})

// Split camel case string and rejoin with spaces
Vue.filter('splitCamel', function(value) {
  if (!value) return ''
  return value.replace(/([a-z0-9])([A-Z])/g, '$1 $2')
})

// Capitalize each word in a string
Vue.filter('capitalize', function(value) {
  if (!value) return ''
  let words = value.toString().split(' ')
  words = words.map(word => {
    return word.charAt(0).toUpperCase() + word.slice(1)
  })
  return words.join(' ')
})

// Split string by the given separator
Vue.filter('split', function(value, separator) {
  if (!value) return ''
  return value.split(separator).join(' ')
})

Vue.filter('formatHashLink', function(hash, location) {
  return { hash: `#${hash}`, ...location }
})
