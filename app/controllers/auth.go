package controllers

import (
	"revel-vuetify-admin/app/models"
	"fmt"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/lujiacn/revauth"
	"github.com/revel/revel"
	"github.com/revel/revel/cache"
	mgodo "gopkg.in/lujiacn/mgodo.v0"
)

type Auth struct {
	*revel.Controller
	mgodo.MgoController
}

// Checks if the session expired by checking if the user identity is still present
func (c *Auth) CheckLogin() revel.Result {
	output := make(map[string]interface{})
	identity, err := c.Session.Get("Identity")
	fmt.Printf("Time:%s; Identity:%s", time.Now().Format(time.RFC3339), identity)
	if err != nil {
		fmt.Println("Session expired")
		output["success"] = false
		return c.RenderJSON(output)
	}

	user := new(models.User)

	// get user information from cache by using the session ID retrieved from the cookie
	if err := cache.Get(c.Session.ID(), &user); err != nil {
		fmt.Println("user not found in cache")

		do := mgodo.New(c.MgoSession, user)
		do.Query = bson.M{"Identity": identity.(string)}

		if err := do.GetByQ(); err != nil {
			fmt.Println("no matched account found in db")
			output["success"] = false
			return c.RenderJSON(output)
		}

		// set the user information in cache
		go cache.Set(c.Session.ID(), user, cache.DefaultExpiryTime)
	}

	output["success"] = true
	output["user"] = user
	return c.RenderJSON(output)
}

// Accepts form-data and returns user information on authentication success
func (c *Auth) Authenticate(account, password string) revel.Result {
	output := make(map[string]interface{})
	if account == "" || password == "" {
		output["success"] = false
		output["error"] = "Account and password cannot be empty."
		return c.RenderJSON(output)
	}
	/*
		type AuthReply struct {
			IsAuthenticated bool
			Error           string
			Account         string
			Name            string
			First           string
			Last            string
			Mail           string
			Depart          string
			Avatar          string
		}
	*/
	authReply := revauth.Authenticate(account, password)

	// authentication failed
	if !authReply.IsAuthenticated {
		output["success"] = false
		output["error"] = authReply.Error
		return c.RenderJSON(output)
	}

	fmt.Println("isAuthenticated!")

	// save the user identity in the session
	// HttpOnly flag is set to true by default; Expiration is set to 24h and could be configured via session.expires
	c.Session["Identity"] = strings.ToLower(account)

	user := new(models.User)
	user.Identity = strings.ToLower(account)
	user.Mail = authReply.Email
	user.Avatar = authReply.Avatar
	user.Name = authReply.Name
	user.First = authReply.First
	user.Last = authReply.Last
	user.Depart = authReply.Depart

	// save authorized user information to db by calling SaveUser defined in revauth
	go func(user *models.User) {
		s := mgodo.NewMgoSession()
		defer s.Close()
		err := user.SaveUser(s)
		if err != nil {
			revel.AppLog.Errorf("Save user error: %v", err)
		}
	}(user)

	// cache user information by using session ID as key, DefaultExpiryTime is one hour by default
	// ID() creates a time-based UUID identifying this session
	go cache.Set(c.Session.ID(), user, cache.DefaultExpiryTime)

	output["success"] = true
	output["user"] = user
	return c.RenderJSON(output)
}

// Logout, clear session variables and delete the user information from cache
func (c *Auth) Logout() revel.Result {
	c.Session = make(map[string]interface{})
	go cache.Delete(c.Session.ID())

	output := map[string]interface{}{
		"success": true,
	}
	return c.RenderJSON(output)
}
