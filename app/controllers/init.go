package controllers

import (
	"net/http"

	revel "github.com/revel/revel"
	mgodo "gopkg.in/lujiacn/mgodo.v0"
)

func init() {
	mgodo.MgoControllerInit()
	// revel.InterceptFunc(checkUser, revel.BEFORE, &App{})
	revel.InterceptFunc(checkUser, revel.BEFORE, &Base{})
}

// Check if a user is authorized
func checkUser(c *revel.Controller) revel.Result {
	if _, err := c.Session.Get("Identity"); err != nil {
		c.Response.WriteHeader(http.StatusUnauthorized, "application/json")
		return c.RenderText("401 Unauthorized")
	}
	return nil
}
