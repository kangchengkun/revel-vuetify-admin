package controllers

import (
	"encoding/json"
	"net/http"
	"net/url"

	"github.com/revel/revel"
	mgodo "gopkg.in/lujiacn/mgodo.v0"
)

type Base struct {
	*revel.Controller
	mgodo.MgoController
}

type response struct {
	Data    interface{} `json:"data,omitempty"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
}

const (
	OK = iota
	BAD_REQUEST
	LOGIN_FAILED
	SESSION_EXPIRED
	MYSQL_ERROR
	SERVER_ERROR
	GRPC_SERVICE_ERROR
)

func StatusText(code int) string {
	m := map[int]string{
		OK:                 "success",
		BAD_REQUEST:        "bad request",
		LOGIN_FAILED:       "login failed",
		SESSION_EXPIRED:    "session expired",
		MYSQL_ERROR:        "MySQL error",
		SERVER_ERROR:       "server error",
		GRPC_SERVICE_ERROR: "gRPC service error",
	}
	if v, found := m[code]; found {
		return v
	}
	return ""
}

func (c *Base) Apply(req *revel.Request, resp *revel.Response) {}

func (c *Base) ResponseSuccess(data ...interface{}) *Base {
	res := response{
		Code:    OK,
		Message: "",
	}
	if len(data) > 0 && data[0] != nil {
		res.Data = data[0]
	}
	b, err := json.Marshal(res)
	if err != nil {
		// TODO: handle error
		revel.AppLog.Errorf("Unable to marshal response, %v", err)
		return c
	}
	c.Response.WriteHeader(http.StatusOK, "application/javascript; charset=utf-8")
	if _, err = c.Response.GetWriter().Write(b); err != nil {
		revel.AppLog.Errorf("Response write failed: %v", err)
	}
	return c
}

func (c *Base) ResponseError(code int, message ...string) *Base {
	res := response{
		Code:    code,
		Message: StatusText(code),
	}
	if len(message) > 0 {
		res.Message = message[0]
	}
	b, err := json.Marshal(res)
	if err != nil {
		// TODO: handle error
		revel.AppLog.Errorf("Unable to marshal response, %v", err)
		return c
	}
	c.Response.WriteHeader(http.StatusOK, "application/javascript; charset=utf-8")
	if _, err = c.Response.GetWriter().Write(b); err != nil {
		revel.AppLog.Errorf("Response write failed: %v", err)
	}
	return c
}

// Unmarshal the posted JSON data and store the result in the value pointed to by v
func (c *Base) getJSON(v interface{}) error {
	jsonBlob := c.Params.JSON
	err := json.Unmarshal(jsonBlob, &v)
	if err != nil {
		return err
	}
	return nil
}

func (c *Base) GetJSONData() (map[string]interface{}, error) {
	var jsonData map[string]interface{}
	jsonBlob := c.Params.JSON
	err := json.Unmarshal(jsonBlob, &jsonData)
	if err != nil {
		return nil, err
	}
	return jsonData, nil
}

func (c *Base) getReqArgs(values url.Values) map[string]interface{} {
	out := map[string]interface{}{}
	for k, v := range values {
		out[k] = v[0]
	}
	return out
}

func getReqArgsArray(args url.Values) map[string]interface{} {
	out := map[string]interface{}{}
	for k, v := range args {
		out[k] = v
	}
	return out
}
