package app

import (
	"net/http"
	"revel-vuetify-admin/app/models"
	"revel-vuetify-admin/gateway/gateway-server"

	"github.com/gorilla/mux"
	"github.com/lujiacn/revauth"
	"github.com/revel/revel"
	mgodo "gopkg.in/lujiacn/mgodo.v0"
)

var (
	// AppVersion revel app version (ldflags)
	AppVersion string

	// BuildTime revel app build-time (ldflags)
	BuildTime string
)

func init() {
	// Filters is the default set of global filters.
	revel.Filters = []revel.Filter{
		revel.PanicFilter,             // Recover from panics and display an error page instead.
		revel.RouterFilter,            // Use the routing table to select the right Action
		revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
		revel.ParamsFilter,            // Parse parameters into Controller.Params.
		revel.SessionFilter,           // Restore and write the session cookie.
		revel.FlashFilter,             // Restore and write the flash cookie.
		// csrf.CSRFFilter,
		revel.ValidationFilter,  // Restore kept validation errors and save new ones from cookie.
		revel.I18nFilter,        // Resolve the requested language
		HeaderFilter,            // Add some security based headers
		revel.InterceptorFilter, // Run interceptors around the action.
		revel.CompressFilter,    // Compress the result.
		revel.BeforeAfterFilter, // Call the before and after filter functions
		revel.ActionInvoker,     // Invoke the action.
	}

	// Register startup functions with OnAppStart (order dependent)
	revel.OnAppStart(revauth.Init)
	revel.OnAppStart(mgodo.Init, 0)
	revel.OnAppStart(models.Init, 1)
	revel.OnAppStart(gateway.Init)
	revel.OnAppStart(installHandlers)
}

// HeaderFilter adds common security headers
// There is a full implementation of a CSRF filter in
// https://github.com/revel/modules/tree/master/csrf
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {
	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")
	// c.Response.Out.Header().Add("Referrer-Policy", "strict-origin-when-cross-origin")
	c.Response.Out.Header().Add("Access-Control-Allow-Origin", "*") // for temporary use only

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

func installHandlers() {
	revel.AddInitEventHandler(func(event revel.Event, i interface{}) revel.EventResponse {
		if event == revel.ENGINE_STARTED {
			revelHandler := revel.CurrentEngine.(*revel.GoHttpServer).Server.Handler
			r := mux.NewRouter()
			r.PathPrefix("/api/v1/").Handler(authCheck(gateway.Mux)) // matches "/api/v1/*"
			r.PathPrefix("/").Handler(revelHandler)                      // catch-all handler
			revel.CurrentEngine.(*revel.GoHttpServer).Server.Handler = r
		}
		return 0
	})
}

func authCheck(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c := newController(w, r)
		cse := revel.NewSessionCookieEngine()
		cse.Decode(c)
		if _, err := c.Session.Get("Identity"); err != nil {
			w.Header().Set("Content-Type", "application/json; charset=utf-8")
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("401 Unauthorized"))
			return
		}
		h.ServeHTTP(w, r)
	})
}

func newController(w http.ResponseWriter, r *http.Request) *revel.Controller {
	context := revel.NewGoContext(nil)
	context.Request.SetRequest(r)
	context.Response.SetResponse(w)
	c := revel.NewController(context)
	return c
}
