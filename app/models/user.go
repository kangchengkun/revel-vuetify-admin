package models

import (
	revauth "github.com/lujiacn/revauth/app/models"
)

// User is alias of revauth.User
/*
type User struct {
	mgodo.BaseModel `bson:",inline"`
	Identity        string `bson:"Identity,omitempty"` //if ldap is SAMAccount
	Name            string `bson:"Name,omitempty"`
	First           string `bson:"First,omitempty"`
	Last            string `bson:"Last,omitempty"`
	Mail            string `bson:"Mail,omitempty"`
	Depart          string `bson:"Depart,omitempty"`
	Avatar          string `bson:"Avatar,omitempty"`
}
*/
type User = revauth.User
