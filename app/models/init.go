package models

import (
	"fmt"
	"log"
	"os"

	mgodo "gopkg.in/lujiacn/mgodo.v0"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
)

var (
	DB     *gorm.DB
	DBName string
)

func Init() {
	modelInit()
	dbInit()
}

func modelInit() {
	s := mgodo.NewMgoSession()
	// ensureIndex(s)
	defer s.Close()
}

// init MySQL database connection
func dbInit() {
	dbHost := revel.Config.StringDefault("db.host", "")
	dbPort := revel.Config.StringDefault("db.port", "")
	dbUser := revel.Config.StringDefault("db.user", "")
	dbPass := revel.Config.StringDefault("db.pass", "")
	dbName := revel.Config.StringDefault("db.name", "")

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true", dbUser, dbPass, dbHost, dbPort, dbName)

	var err error
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	if revel.RunMode == "dev" {
		db.LogMode(true)
		log.New(os.Stdout, "\r\n", log.LstdFlags)
	}

	DB = db
	DBName = dbName
}
