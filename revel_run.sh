#!/usr/bin/env sh
go get -u github.com/revel/cmd/revel

unset http_proxy
unset https_proxy

export http_address="localhost"
export http_port="9090"
export mongodb_dial="mongodb://localhost/admin"
export mongodb_name="revel-vuetify-admin"
export grpcauth_port="50051"
# export grpcauth_server="166.118.154.4"
export grpcauth_server="bicc.sanofi.com"

# admin prod
export grpc_port="5082"
export grpc_server="7.42.80.220"

# revel run [import-path] [run-mode]
revel run -a ./ -m dev
