#!/usr/bin/env sh
go get -u github.com/revel/cmd/revel

unset http_proxy
unset https_proxy

export http_address="0.0.0.0"
export http_port="9090"
export mongodb_dial="mongodb://root:root@mongo:27017/admin"
export mongodb_name="revel-vuetify-admin"
export grpcauth_port="50051"
export grpcauth_server="bicc.sanofi.com"

# MySQL dev
export db_host=localhost
export db_port=3306
export db_user=root
export db_pass=root
export db_name=revel-vuetify-admin

# admin dev
export grpc_port="5082"
export grpc_server="7.42.80.220"

# revel run [import-path] [run-mode] [port]
revel run -a ./ -m dev
