#!/usr/bin/env sh
go get -u github.com/revel/cmd/revel

unset http_proxy
unset https_proxy

export http_address="localhost"
export http_port="9090"
export mongodb_dial="mongodb://localhost:27017/?readPreference=primary&authSource=admin&ssl=false"
export mongodb_name="revel-vuetify-admin"
export grpcauth_port="50051"
export grpcauth_server="bicc.sanofi.com"

# MySQL dev
export db_host=localhost
export db_port=3306
export db_user=root
export db_pass=root
export db_name=revel-vuetify-admin

# admin dev
export grpc_port="5082"
export grpc_server="7.42.80.220"

# run revel app
revel run -a ./ -m dev
