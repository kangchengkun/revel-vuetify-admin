---
name: Proposal
about: Any advice for this web application
title: "[Proposal]"
labels: ''
assignees: ''

---

#### Description [describe your advice]

#### Solution [if any solutions, describe here]

#### Others [screenshots and other info]
