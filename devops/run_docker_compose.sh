docker-compose -f ./docker-compose.yml down -v
docker-compose rm
docker ps -a | grep Exit | grep devops_vue_run | cut -d ' ' -f 1 | xargs docker rm
docker volume rm devops_file_folder_path
docker volume rm devops_vue_app_path
docker-compose -f ./docker-compose.yml up -d
