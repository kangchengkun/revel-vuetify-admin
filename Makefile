#
# Generate go, grpc-gateway, swagger output for v1. E.g.: make gen svc=admin version=v1 pkg=users
gen:
	protoc --proto_path=gateway/$(svc) gateway/$(svc)/$(pkg)/$(version)/*.proto \
	--go_opt=paths=source_relative \
	--go_out=plugins=grpc:gateway/$(svc) \
	--grpc-gateway_out=logtostderr=true,paths=source_relative,grpc_api_configuration=gateway/$(svc)/$(pkg)/$(version)/api_config_http.yaml:gateway/$(svc) \
	--swagger_out=logtostderr=true,grpc_api_configuration=gateway/$(svc)/$(pkg)/$(version)/api_config_http.yaml,allow_merge=true,merge_file_name=$(svc).$(pkg).$(version).swagger:swagger

# Clean go, grpc-gateway, swagger output for v1. E.g.: make clean svc=admin version=v1 pkg=users
clean:
	rm gateway/$(svc)/$(pkg)/$(version)/*.go swagger/$(svc).$(pkg).$(version).*.json

# Generate .pb.go file only. E.g.: make gen_pb svc=admin pkg=common
gen_pb:
	protoc --proto_path=gateway/$(svc) gateway/$(svc)/$(pkg)/*.proto \
	--go_opt=paths=source_relative \
	--go_out=plugins=grpc:gateway/$(svc) 

# Delete .pb.go file only. E.g. make clean_pb svc=admin pkg=common
clean_pb:
	rm gateway/$(svc)/$(pkg)/*.go